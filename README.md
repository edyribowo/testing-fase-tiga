# Blibli-Testing-Pulsa-and-Data

![alt text](https://cdn-2.tstatic.net/tribunnews/foto/bank/images/logo-blibli_20161109_192318.jpg)

Testing framework on the Web App and Android App Blibli.com Digital Product (Credit and Data).

### Prerequisites

* [Java 8](https://www.java.com/en/download/)
* [Jenkins](https://www.jenkins.io/)
* [Appium](http://appium.io/) 
* Webdriver (Chromedriver, Geckodriver, etc)



## Getting Started

### Web Apps
To run on your local machine just simply: 

#### UI Test
```
mvn clean verify -Dtags="@Dekstop"
```
#### API Test
```
mvn clean verify -Dtags="@API"
```
#### DATALAYER Test
```
mvn clean verify -Dtags="@Tracker"
```
You can also change configuration webdriver on serenity.properties.
If you want to change webdriver(you can change it with opera, chrome and firefox):
```
localBrowser=chrome
```
If you want to run in remote webdriver:
```
isRemote=true
```

### Android Apps
Before you run the android, make sure Appium Server and Device is connected
To run on your local machine just simply: 
```
mvn clean verify -Dtags="@Mobile"
```


## Automation Tools

### Build Tools
* GitHub
* Maven
* Jenkins

### Deployment Technology
Included in pom.xml for maven dependency
* Serenity
* Appium
* Selenium
git 
#### Depedency Support
* Serenity
* Appium
* Selenium
* Spring Boot
* Jackson
* Bonigarcia
* Javamail
* Jinjava

## Features
### UI Features
* Buy Phone Credit Prabayar, Pascabayar (with and without injected cookie)
* Buy Package Data
* History Order
* Login
* Promo Page
* Verify Component in Digital Product

### API Feature
* Digital Content
* Digital Product Operator
* Provider
* Repurchase Order
* User
* History Number
* Login

### DataLayer Feature
* Impression on Digital Product
* checkOutDetail on Payment Page

## Suites Capabilities
* Multiple Test
* Parallel Execution Test
* Multiplatform Parallel Execution Test (e.g. Android & Web at the same time)

## Examples

### Buy Phone Credit:

#### Web App Automation
![alt text](https://i.ibb.co/kSg9G5Y/Selection-852.png)

#### Android and Web App at The Same Time
![alt text](https://i.ibb.co/KbMSCF8/Selection-853.png)

## Authors

* **Edy Ribowo** - *Future Fase 3* - [PurpleBooth](https://github.com/edyribowo)
