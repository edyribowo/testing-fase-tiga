@Mobile @DataPackageMobile
Feature: Flow buy package data from login until user click paynow

  Background: User already login
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    And   [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    And   [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Semua on homepage
    Then  [mobile-apps] user on Category page

  @Positive @UI @SanityMobile
  Scenario Outline: Buy Package Data with number <phoneNumber>
    When  [mobile-apps] user click the Paket Data icon
    Then  [mobile-apps] user on Paket Data Page
    When  [mobile-apps] user input number with <phoneNumber>
    And   [mobile-apps] users select nominal pulsa
    And   [mobile-apps] user select <nominal>
    And   [mobile-apps] user click button "Lanjut ke pembayaran"
    Then  [mobile-apps] user on Payment Pages

    Examples:
      | phoneNumber | nominal              |
      | 08884069314 | Internet Volume 4 GB |

  @Negative @UI @SanityMobile
  Scenario Outline: Buy Package Data with number <phoneNumber>
    When  [mobile-apps] user click the Paket Data icon
    Then  [mobile-apps] user on Paket Data Page
    When  [mobile-apps] user input number with <phoneNumber>
    Then  [mobile-apps] user see warning <errorMessage>
    Examples:
      | phoneNumber      | errorMessage               |
      | 6345786587589248 | Tampaknya nomor itu salah. |