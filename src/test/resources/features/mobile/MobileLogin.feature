@Mobile @LoginMobile
Feature: Trying to mobile

  @Positive @SanityMobile
  Scenario: Blibli Login Mobile until logout Automation
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    Then  [mobile-apps] user on Profile page
    When  [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    When  [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Akun button
    Then  [mobile-apps] user on Profile page
    And   [mobile-apps] username is correct
    When  [mobile-apps] user click toogle in Profil
    And   [mobile-apps] user click Keluar in Profil
    Then  [mobile-apps] user click Keluar in PopUp

  @Negative
  Scenario Outline: User login with <email> and <password>
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    Then  [mobile-apps] user on Profile page
    When  [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type <email> and <password>
    And   [mobile-apps] user click Masuk button
    Then  [mobile-apps]  user able to see error message <errorMessage>
    Examples:
      | email             | password      | errorMessage |
      |                   |               | Wajib diisi. |
      |                   | Edy30121ghg99 | Wajib diisi. |
      | eribowo@gmail.com |               | Wajib diisi. |




