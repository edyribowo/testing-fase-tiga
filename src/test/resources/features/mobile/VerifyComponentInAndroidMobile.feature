@Mobile @MobileVerifyComponent
Feature: Verify Component in Mobile Digital Product Page
  Background: User already login
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    And   [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    And   [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Semua on homepage
    And   [mobile-apps] user on Category page
    Then  [mobile-apps] user click the Pulsa icon

  @Positive @UI @SanityMobile
  Scenario Outline: check phone book component
    When  [mobile-apps] user click phonebook icon on search bar
    And   [mobile-apps] user click allow permission to access contact
    When  [mobile-apps] user click select <contact name>
    Then  [mobile-apps] the <phone number> should be appear in digital product page
    Examples:
      | contact name  | phone number |
      | A Dummy       | 083833685129 |

  @Positive @UI @SanityMobile
  Scenario: check semua-info component
    When  [mobile-apps] user click semua-info component
    Then  [mobile-apps] user is on Digital Product FAQ Page