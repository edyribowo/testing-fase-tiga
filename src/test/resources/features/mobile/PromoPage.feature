@Mobile @MobilePromoPage
Feature: Flow check promo page on mobile apps

  Background: User already login
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    And   [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    And   [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Semua on homepage
    Then  [mobile-apps] user on Category page

  @Positive @UI @SanityMobile
  Scenario Outline: check promo page on <category>
    When  [mobile-apps] user click the <category> icon
    Then  [mobile-apps] user on <category> Page
    When  [mobile-apps] user click banner promo
    Then  [mobile-apps] user on promo page
    Examples:
      | category   |
      | Paket Data |