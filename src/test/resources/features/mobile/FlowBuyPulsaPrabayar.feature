@Mobile @PulsaPrabayar
Feature: Flow buy pulsa Pra Bayar from login until user click beli pulsa

  Background: User already login
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    And   [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    And   [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Semua on homepage
    Then  [mobile-apps] user on Category page

  @Positive @UI @SanityMobile
  Scenario Outline: Buy Pulsa with number <phoneNumber>
    When  [mobile-apps] user click the Pulsa icon
    Then  [mobile-apps] user on Pulsa Prabayar Page
    When  [mobile-apps] user input number with <phoneNumber>
    And   [mobile-apps] users select nominal pulsa
    And   [mobile-apps] user select <nominal>
    And   [mobile-apps] user click button "Lanjut ke pembayaran"
    Then  [mobile-apps] user on Payment Pages
    And   [mobile-apps] make sure the <phoneNumber> is correct
    When   [mobile-apps] user click back navigation
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Akun button
    And  [mobile-apps] user click toogle in Profil
    And   [mobile-apps] user click Keluar in Profil
    Then  [mobile-apps] user click Keluar in PopUp

    Examples:
      | phoneNumber | nominal |
      | 08884069314 | 100.000 |
#      | 081249078442 | 50.000  |

  @Negative @ErrorMessage @UI
  Scenario Outline: Buy Pulsa with number <phoneNumber>
    When  [mobile-apps] user click the Pulsa icon
    Then  [mobile-apps] user on Pulsa Prabayar Page
    When  [mobile-apps] user input number with <phoneNumber>
    Then  [mobile-apps] user see warning <errorMessage>
    Examples:
      | phoneNumber      | errorMessage               |
      | 6345786587589248 | Tampaknya nomor itu salah. |