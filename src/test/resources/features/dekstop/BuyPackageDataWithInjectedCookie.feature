@Dekstop @BuyDataPackageinjectCookieFeature
Feature: Flow buy data with inject cookie from login until user click beli pulsa

  @SaveCookie
  Scenario: User login with valid email and password
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click login button
    And   [login-page] user typing email and password
    And   [login-page] user click login button
    And   [login-page] user click send verification email button
    And   [email] set email with email and password
    And   [login-page] user fill verification code
    And   [login-page] user click verification
    Then  [verification-page] user able to see verif later link on verification page
    And   [login-page] user storage the cookie
    When  [verification-page] user click verif later link
    And   [home-page] user click skip point
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button

  @Positive @UI @Sanity
  Scenario Outline: Buy Package data with <provider>
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] user click phone credit tab
    And   [pulsa-page] if any pop up, user close the pop up

    And   [pulsa-page] user select sub menu paket data
    Then  [pulsa-page] user able to see option paket data
    When  [pulsa-page] user select <paket data> in option
    And   [pulsa-page] user click Beli Pulsa
    Then  [payment-page] verify user able to see payment page

    Examples:
      | phoneNumber  | paket data                    | provider  |
      | 081249078442 | Data 50.000                   | Telkomsel |
      | 08886198492  | Kuota Nonstop 18GB            | Smartfren |
#      | 083833685129 | BRONET 24 Jam 8GB             | Axis      |
#      | 085854267899 | Freedom Apps FUN 2GB / 7 Hari | Indosat   |

