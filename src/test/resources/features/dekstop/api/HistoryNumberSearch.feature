@API @GetHistoryNumberSearch
Feature: Verify Get History Number purchase from user

  Background: User already login
    Given user prepare data username with demologinblibli123@gmail.com
    And   user prepare data password with sukasukaayamoliv123
    And   user send request data login
    Then  login status should be '400'

    When  user get token from response
    And   set verification code to Payload
    And   user send request token to request-challenge
    Then  request challenge-code status code should be '200'

    When  [email] set email with existing email demologinblibli123@gmail.com and sukasukaayamoliv123
    And   user get kode verifikasi from email
    And   user send request data login after verification
    Then  request send kode verif status status code should be '200'

  @Positive @Sanity
  Scenario Outline: Verify Get History Number purchase from user
    Given prepare data to get repurchased order with product type <productType>
    When  send get history number request
    Then  get history number status code must be '200'
    And   the <phoneNumber> have to on list in data response

    Examples:
      | productType  | phoneNumber  |
      | PHONE_CREDIT | 081249078442 |