@API
Feature: Verify Digital Product Operator

  @DigitalProductOperator @Sanity
  Scenario Outline: Verify API Digital Product Operator with <product>
    Given send request data with <product>
    Then  get response product operator status code must be '200'
    And   the size of data should be <number>
    Examples:
      | product               | number |
      | ENTERTAINMENT_VOUCHER | 5      |
      | GAME_VOUCHER_TOPUP    | 29     |
      | GAME_VOUCHER_TOPUP    | 29     |