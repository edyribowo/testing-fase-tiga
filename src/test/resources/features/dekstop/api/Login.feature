@API @LoginAPI
Feature: Verify Login By API

  @Positive @Sanity
  Scenario Outline: Verify login API with email : <email>
    Given user prepare data username with <email>
    And   user prepare data password with <password>
    And   user send request data login
    Then  login status should be '400'

    When  user get token from response
    And   set verification code to Payload
    And   user send request token to request-challenge
    Then  request challenge-code status code should be '200'

    When  [email] set email with existing email <email> and <passwordgmail>
    And   user get kode verifikasi from email
    And   user send request data login after verification
    Then  request send kode verif status status code should be '200'

    Then  the 'id' should be <id>
    And   the 'username' should be <username>

    Examples:
      | email                        | password            | passwordgmail       | id        | username                     |
#      | demologinblibli123@gmail.com | sukasukaayamoliv123 | sukasukaayamoliv123 | 536652627 | demologinblibli123@gmail.com |
      | futurepengirim@gmail.com     | sukasukaayamoliv123 | sukasukaayamoliv123 | 532929515 | futurepengirim@gmail.com     |
#      | futureedy95@gmail.com    | asdfghjkl123        | sukasukaayamoliv123 | 532929515 | 081973862176 |
#      | alexandriammm915@gmail.com | lala123456 | Zada123!      | 532929515 | 081973862176 |

  @Negative
  Scenario Outline: Verify login API with email : <email>
    Given user prepare data username with <email>
    And   user prepare data password with <password>
    And   user send request data login
    Then  login status should be '400'
    And   the login status should be 'UNAUTHORIZED'
    Examples:
      | email                    | password      |
      | futurepenerima@gmail.com | syalala12345  |
      | stony7391@gmail.com      | cobalagiya123 |

  @Negative
  Scenario Outline: Verify login with locked user
    Given user prepare data username with <email>
    And   user prepare data password with <password>
    And   user send request data login
    Then  login status should be '400'
    And   the login status should be 'BAD_REQUEST'
    Examples:
      | email                    | password            |
      | futurepenerima@gmail.com | sukasukaayamoliv123 |
