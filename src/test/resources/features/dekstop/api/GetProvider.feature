@API
Feature: Get Provider

  @GetProvider @Sanity
  Scenario: Verify the number of product in phone credit
    Given prepare data to get provider with customer number '<customerNumber>'
    And prepare data to get provider with product type '<productTypePulsa>'
    When  send get provider request
    Then  get provider status code must be '200'
    And   get provider response equals with request