@API
Feature: A Get User Data after login

  @Positive @GetSingleUserFeature @Sanity
  Scenario Outline: Verify login API with email : <email>
    Given user prepare data username with <email>
    And   user prepare data password with <password>
    And   user send request data login
    Then  login status should be '400'

    When  user get token from response
    And   set verification code to Payload
    And   user send request token to request-challenge
    Then  request challenge-code status code should be '200'

    When  [email] set email with existing email <email> and <passwordgmail>
    And   user get kode verifikasi from email
    And   user send request data login after verification
    Then  request send kode verif status status code should be '200'

    When  send get single user request
    Then  get single user status code must be '200'

    Then  the 'id' should be <id>
    And   the 'username' should be <username>
    And   the 'email' should be <email>
    And   the 'phoneNumberVerified' should be <phoneNumberVerified>
    And   the 'needVerifiedPhoneNumber' should be <needVerifiedPhoneNumber>
    And   the 'emailVerified' should be <emailVerified>

    Examples:
      | email                        | password            | passwordgmail       | id        | username                     | phoneNumberVerified | needVerifiedPhoneNumber | emailVerified |
      | demologinblibli123@gmail.com | sukasukaayamoliv123 | sukasukaayamoliv123 | 536652627 | demologinblibli123@gmail.com | false               | true                    | true          |



