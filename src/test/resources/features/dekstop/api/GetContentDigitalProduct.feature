@API @GetContentDigitalProduct
Feature: Get content of digital product

  @GetContentDigitalProduct @Sanity
  Scenario: Get content of digital product
    Given send get content digital product request
    Then  get content digital product status code must be '200'
    And   get content response equals with request
