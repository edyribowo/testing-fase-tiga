@API
Feature: Verify GetRepurchasedOrder

  Background: User already login
    Given user prepare data username with futureedy95@gmail.com
    And   user prepare data password with asdfghjkl123
    And   user send request data login
    Then  login status should be '400'

    When  user get token from response
    And   set verification code to Payload
    And   user send request token to request-challenge
    Then  request challenge-code status code should be '200'

    When  [email] set email with existing email futureedy95@gmail.com and sukasukaayamoliv123
    And   user get kode verifikasi from email
    And   user send request data login after verification
    Then  request send kode verif status status code should be '200'

  @Positive @GetRepurchasedOrder @Sanity
  Scenario Outline: Get repurchased order with product type <productType>
    Given prepare data to get repurchased order with product type <productType>
    When  send get repurchased order request
    Then  get repurchased order status code must be '200'
    And   the repurchased 'customerId' should be <customerId>
    And   the repurchased 'sku' should be <sku>
    And   the repurchased 'operatorName' should be <operatorName>
    And   the repurchased 'productType' should be <productType>

    Examples:
      | productType  | customerId  | sku                   | operatorName | productType  |
      | DATA_PACKAGE | 08886198492 | BLP-25978-XDP-0000310 | Smartfren    | DATA_PACKAGE |
