@Compare @AvailableProduct
Feature: Verify Promo Page on Digital Product Page

  @CompareDesktop
  Scenario Outline: Verify availabel product with number <provider>
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click login button
    And   [login-page] user typing email and password
    And   [login-page] user click login button
    And   [login-page] user click send verification email button
    And   [email] set email with email and password
    And   [login-page] user fill verification code
    And   [login-page] user click verification
    Then  [verification-page] user able to see verif later link on verification page
    And   [login-page] user storage the cookie
    When  [verification-page] user click verif later link
    And   [home-page] user click skip point
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button

    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    Then  user get size of AvProdDesktop from desktop
    And   prepare to add AvailableProduct data comparation AvProdDesktop on Desktop
    Examples:
      | provider | phoneNumber |
#      | Telkomsel | 081249078442 |
      | Axis     | 08383685129 |

  @CompareMobile
  Scenario Outline: Verify availabel product with number <provider>
    Given [mobile-apps] user Open "Blibli" Application
    When  [mobile-apps] user click Akun button
    And   [mobile-apps] user click Masuk/Daftar
    And   [mobile-apps] user click None Of The Above Google Smart Lock
    And   [mobile-apps] user type email
    And   [mobile-apps] user type password
    And   [mobile-apps] user click Masuk button
    When  [mobile-apps] user click Verification Later
    And   [mobile-apps] user click back navigation
    And   [mobile-apps] user click Semua on homepage
    Then  [mobile-apps] user on Category page

    When  [mobile-apps] user click the Pulsa icon
    Then  [mobile-apps] user on Pulsa Prabayar Page
    When  [mobile-apps] user input number with <phoneNumber>
    And   [mobile-apps] users select nominal pulsa
    Then  user get size of AvProdMobile from mobile
    And   prepare to add AvailableProduct data comparation AvProdMobile on Mobile
    Examples:
      | provider  | phoneNumber  |
#      | Telkomsel | 081249078442 |
      | Axis      | 083833685129 |

  @CompareAPI
  Scenario Outline: Verify available product with number <provider> on API
    Given prepare data to get providers with customer number <customerNumber>
    And prepare data to get providers with product type <productType>
    When  send get provider request
    Then  get provider status code must be '200'
    And   user get size available product from API
    And   user get size of AvProdAPI from API
    And   prepare to add AvailableProduct data comparation AvProdAPI on API
    Examples:
      | provider | customerNumber | productType  |
#      | Telkomsel | 081249078442   | PHONE_CREDIT |
      | Axis      | 08383685129    | PHONE_CREDIT |

  @DoCompareAvailableProduct
  Scenario Outline: Do compare data on Mobile, API, and Desktop
    Given prepare to read the excel file
    When  prepare to add AvailableProduct data comparation Available Pulsa Product on Data
    And   prepare to add AvailableProduct data comparation <Provider> on Provider
    Then  The Available Pulsa Product from the three platform should be same
    Examples:
      | Provider |
#      | Telkomsel |
      | Axis     |


