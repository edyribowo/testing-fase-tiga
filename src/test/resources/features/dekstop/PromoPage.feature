@Dekstop @PromoPageFeature @Demo
Feature: Verify Promo Page on Digital Product Page

  @VerifyPromoPage @Positive @Sanity
  Scenario Outline: Verify the promo page <index>
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user open digitalproduct page
    And   [pulsa-page] user click promo banner <index>
    Then  [pulsa-page] verify the promo page url contains "/promosi"
    Examples:
      | index |
      | 2     |
      | 3     |