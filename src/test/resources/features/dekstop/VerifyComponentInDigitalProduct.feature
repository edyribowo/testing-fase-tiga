@Dekstop @VerifyComponent
Feature: Verify Component in Digital Product Page

  @Positive @ProductNotAvailableComponent @UI @Sanity
  Scenario Outline: Verify ProductNotAvailable Component on DigitalProduct in <product> tab
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button
    And   [home-page] user open digitalproduct page
    And   [pulsa-page] user select <product> on tab
    Then  [pulsa-page] user able to see ProductNotAvailable Component
    Examples:
      | product             |
      | Pulsa & Paket Data  |
      | Pulsa Pascabayar    |
#      | Listrik PLN         |
#      | Tagihan Air         |
#      | Uang Elektronik     |
#      | Telkom              |
#      | Voucher Game        |
#      | Voucher Streaming   |
#      | BPJS Kesehatan      |
#      | TV Kabel & Internet |