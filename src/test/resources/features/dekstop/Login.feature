@Dekstop @LoginFeature @Demo1
Feature: Flow login until user be able to see username on homepage

  @Positive @UserNotVerifyEmail @UI @Sanity
  Scenario: User login with valid email and password
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click login button
    And   [login-page] user typing email and password
    And   [login-page] user click login button
    And   [login-page] user click send verification email button
    And   [email] set email with email and password
    And   [login-page] user fill verification code
    And   [login-page] user click verification
    Then  [verification-page] user able to see verif later link on verification page
    And   [login-page] user storage the cookie
    When  [verification-page] user click verif later link
    And   [home-page] user click skip point
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button
    Then  [homepage] user able to see the account name in homepage

  @Negative @LoginFeature @UI
  Scenario Outline: User login with <email> and <password>
    When  [home-page] user open the blibli homepage
    And   [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button
    And   [home-page] user click login button
    And   [login-page] user type <email> and <password>
    Then  [login-page] button Pay Now disabled
    Examples:
      | email             | password    |
      |                   |             |
      |                   | Edy30111997 |
      | eribowo@gmail.com |             |