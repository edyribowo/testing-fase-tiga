@Dekstop @BuyPulsaPascabayarFeature
Feature: Flow buy pulsa Pasca Bayar from login until user click beli pulsa

#  Background: User already login
#    Given [home-page] user open the blibli homepage
#    When  [home-page] set cookie with gdn-value
#    And   [home-page] user close the iframe if available
#    And   [home-page] user click Beri Izin Lokasi Button
#    And   [home-page] user click login button
#    And   [login-page] user typing email and password
#    And   [login-page] user click login button
#    And   [login-page] user click send verification email button
#    And   [email] set email with email and password
#    And   [login-page] user fill verification code
#    And   [login-page] user click verification
#    And   [home-page] user open digitalproduct page

#  @Positive @ErrorMessage @UI
#  Scenario Outline: Buy Pulsa with number valid number <phoneNumber>
#    When  [home-page] user click the Pulsa icon
#    And   [pulsa-page] user click pulsa post-paid button
#    Then  [pulsa-page] user able to see url contains "pulsa-postpaid" in pulsa page
#    And   [pulsa-page] user able to see form phone number in pulsa page
#    When  [pulsa-page] user fill the form <phoneNumber>
#    And   [pulsa-page] if any pop up, user close the pop up
#    And   [pulsa-page] user click Beli Pulsa
#    Then  [pulsa-page] user able to see error message <errorMessage>
#    Examples:
#      | phoneNumber  | errorMessage               |
#      | 085852773775 | Tagihan ini sudah dibayar. |

  @Negative @ErrorMessage @UI @Sanity
  Scenario Outline: Buy Pulsa with number valid number <phoneNumber>
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    When  [pulsa-page] user click pulsa post-paid button
    Then  [pulsa-page] user able to see url contains "pulsa-postpaid" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] if any pop up, user close the pop up
    Then  [pulsa-page] user able to see error message <errorMessage>
    And   [pulsa-page] button Pay Now disabled
    Examples:
      | phoneNumber      | errorMessage          |
      | 9489327482637468 | Nomornya tidak valid. |

  @Negative @DisabledButton @UI @Sanity
  Scenario Outline: Buy Pulsa with number <phoneNumber>
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user click pulsa post-paid button
    Then  [pulsa-page] user able to see url contains "pulsa-postpaid" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] if any pop up, user close the pop up
    Then  [pulsa-page] button Pay Now disabled
    Examples:
      | phoneNumber |
      |             |
      | 08          |
      | djhcjs      |
