@Dekstop @BuyDataPackageFeature
Feature: Flow buy data from login until user click beli pulsa

  Background: User already login
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button
    And   [home-page] user click login button
    And   [login-page] user typing email and password
    And   [login-page] user click login button
    And   [login-page] user click send verification email button
    And   [email] set email with email and password
    And   [login-page] user fill verification code
    And   [login-page] user click verification
    Then  [verification-page] user able to see verif later link on verification page

  @Positive @UI
  Scenario Outline: Buy Package data with <provider>
    When  [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] user click phone credit tab
    And   [pulsa-page] if any pop up, user close the pop up

    And   [pulsa-page] user select sub menu paket data
    Then  [pulsa-page] user able to see option paket data
    When  [pulsa-page] user select <paket data> in option
    And   [pulsa-page] user click Beli Pulsa
    Then  [payment-page] verify user able to see payment page

    Examples:
      | phoneNumber  | paket data                    | provider  |
      | 081249078442 | Data 50.000                   | Telkomsel |
      | 08886198492  | Kuota Nonstop 18GB            | Smartfren |
#      | 083833685129 | BRONET 24 Jam 8GB             | Axis      |
#      | 085854267899 | Freedom Apps FUN 2GB / 7 Hari | Indosat   |

