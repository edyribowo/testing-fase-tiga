@Tracker @Dekstop @DigitalHomepageImpression
Feature: Verify tracker Impression on Digital Product Page

  @Positive @Sanity
  Scenario Outline: Verify tracker on <product> tab
    Given  [home-page] user open digitalproduct page
    And   [pulsa-page] user select <product> on tab
    Then  [pulsa-page] user able to see ProductNotAvailable Component

    And   [tracker] the event 'digitalHomepageImpression' appears only <ImpressionCount>

    When  [tracker] user extract dataLayer <dataLayerValue>
    Then  [tracker] the 'event' should be <event>
    And   [tracker] the 'pageType' should be <pageType>
    And   [tracker] the 'action' should be <action>
    And   [tracker] the 'category' should be <category>
    And   [tracker] the 'label' should be <label>

    Examples:
      | product             | ImpressionCount | event                     | dataLayerValue             | pageType                   | action                    | category                   | label            |
      | Pulsa & Paket Data  | 1               | digitalHomepageImpression | digital-phone_credit       | digital-phone_credit       | digitalHomepageImpression | digital-phone_credit       | digital products |
      | Listrik PLN         | 1               | digitalHomepageImpression | digital-electricity_credit | digital-electricity_credit | digitalHomepageImpression | digital-electricity_credit | digital products |
      | Tagihan Air         | 1               | digitalHomepageImpression | digital-water_bill         | digital-water_bill         | digitalHomepageImpression | digital-water_bill         | digital products |
      | Uang Elektronik     | 1               | digitalHomepageImpression | digital-eMoney             | digital-eMoney             | digitalHomepageImpression | digital-eMoney             | digital products |
      | Telkom              | 1               | digitalHomepageImpression | digital-telkom             | digital-telkom             | digitalHomepageImpression | digital-telkom             | digital products |
      | Voucher Game        | 1               | digitalHomepageImpression | digital-game_voucher       | digital-game_voucher       | digitalHomepageImpression | digital-game_voucher       | digital products |
      | BPJS Kesehatan      | 1               | digitalHomepageImpression | digital-bpjs               | digital-bpjs               | digitalHomepageImpression | digital-bpjs               | digital products |
      | TV Kabel & Internet | 1               | digitalHomepageImpression | digital-tv_kabel_postpaid  | digital-tv_kabel_postpaid  | digitalHomepageImpression | digital-tv_kabel_postpaid  | digital products |