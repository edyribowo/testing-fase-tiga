@Tracker @TrackerPaymentPage
Feature: Verify tracker on Payment Page

  Background: User already payment page
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] user close the iframe if available
    And   [home-page] user click Beri Izin Lokasi Button
    And   [home-page] user click login button
    And   [login-page] user type futureedy95@gmail.com and asdfghjkl123
    And   [login-page] user click login button
    And   [login-page] user click send verification email button
    And   [email] set email with existing email futureedy95@gmail.com and sukasukaayamoliv123
    And   [login-page] user fill verification code
    And   [login-page] user click verification
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form 081249078442
    And   [pulsa-page] user click phone credit tab
    And   [pulsa-page] if any pop up, user close the pop up

    And   [pulsa-page] user select sub menu paket data
    Then  [pulsa-page] user able to see option paket data
    When  [pulsa-page] user select Data 50.000 in option
    And   [pulsa-page] user click Beli Pulsa
    Then  [payment-page] verify user able to see payment page

    @CheckoutDigital
    Scenario: Verify CheckoutDigital
      When  [tracker] the event 'checkoutDigital' appears only 1

      And   [tracker] user extract dataLayer checkoutDigital

      Then  [tracker] the 'event' should be checkoutDigital
      And   [tracker] the 'pageType' should be digital-
      And   [tracker] the 'action' should be checkoutDigital
      And   [tracker] the 'category' should be digital-

