@Dekstop @BuyPulsaPrabayarFeature
Feature: Flow buy pulsa Pra Bayar from login until user click beli pulsa

#  Background: User already login
#    Given [home-page] user open the blibli homepage
#    When  [home-page] set cookie with gdn-value
#    And   [home-page] user close the iframe if available
#    And   [home-page] user click login button
#    And   [login-page] user typing email and password
#    And   [login-page] user click login button
#    And   [login-page] user click send verification email button
#    And   [email] set email with email and password
#    And   [login-page] user fill verification code
#    And   [login-page] user click verification
#    Then  [verification-page] user able to see verif later link on verification page

  @Positive @UI @BuyValidPulsa @Sanity
  Scenario Outline: Buy Pulsa with number 081249078442
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    When  [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] if any pop up, user close the pop up
    Then  [pulsa-page] user able to see option nominal pulsa
    When  [pulsa-page] user click close verif number banner if any
    And   [pulsa-page] user choose nominal pulsa <nominal>
    And   [pulsa-page] user click Beli Pulsa
    Then  [payment-page] verify user able to see payment page
    Examples:
      | phoneNumber  | nominal   |
      | 081249078442 | 500.000 |


  @Negative @ErrorMessage @UI @Sanity
  Scenario Outline: Buy Pulsa with number <phoneNumber>
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] if any pop up, user close the pop up
    Then  [pulsa-page] user able to see error message <errorMessage>
    Examples:
      | phoneNumber      | errorMessage          |
      | 6345786587589248 | Nomornya tidak valid. |
#      | 9489327482637468 | Nomornya tidak valid. |

  @Negative @DisabledButton @UI @Sanity
  Scenario Outline: Buy Pulsa with number <phoneNumber>
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user able to see form phone number in pulsa page
    When  [pulsa-page] user fill the form <phoneNumber>
    And   [pulsa-page] if any pop up, user close the pop up
    Then  [pulsa-page] button Pay Now disabled
    Examples:
      | phoneNumber |
      |             |
      | 08          |
      | djhcjs      |