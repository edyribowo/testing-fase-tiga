@Dekstop @HistoryOrderFeature
Feature: Verify history order from user that purchased before

  @VerifyHistoryOrderElement @Positive @Sanity
  Scenario Outline: Verify history order from user that purchased before
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    And   [pulsa-page] user can see the <number> on history order
    Examples:
      | number      |
      | 08886198492 |

  @VerifyOrderByHistoryOrder @Positive @Sanity
  Scenario Outline: Verify create order by history order from user that purchased before
    Given [home-page] user open the blibli homepage
    When  [home-page] set cookie with gdn-value
    And   [home-page] use cookie from storage
    And   [home-page] user open the blibli homepage
    And   [home-page] user open digitalproduct page
    Then  [pulsa-page] user able to see url contains "pulsa" in pulsa page
    When  [pulsa-page] click history order
    Then  [pulsa-page] verify the number is <number> on input form
    And   [pulsa-page] if any pop up, user close the pop up
    And   [pulsa-page] user able to see option nominal pulsa
    When  [pulsa-page] user choose nominal pulsa <nominal>
    And   [pulsa-page] user click Beli Pulsa
    Then  [payment-page] verify user able to see payment page

    Examples:
      | number      | nominal |
      | 08886198492 | 200.000 |