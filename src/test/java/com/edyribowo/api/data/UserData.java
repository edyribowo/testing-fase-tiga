package com.edyribowo.api.data;

import io.restassured.response.Response;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("com.edyribowo.api.data.UserData")
@Data
public class UserData {
    private int id;
    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private boolean phoneNumberVerified;
    private boolean emailVerified;

    private Response sendRequestDataLoginResponse;
    private Response sendRequestChallengeCodeResponse;
    private Response sendRequestSecondDataLoginResponse;
    private Response detailUserResponse;
    private Response loginResponse;
    private Response purchaseHistoryNumber;

    private Map<String, String> cookies;
    private Map<String, String> cookiesAfterVerification;
    private Map<String, String> cookiesAfterLogin;
}
