package com.edyribowo.api.data;

import io.restassured.response.Response;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.api.data.ProviderData")
@Data
public class ProviderData {
    private String provider;
    private String customerNumber;
    private String productType;
    private int sizeProductAvailable;

    private Response repurchaseOrderResponse;
    private Response productOperatorResponse;
}
