package com.edyribowo.api.data;

import lombok.Data;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.api.data.DigitalProductDataLayer")
@Data
public class DigitalProductDataLayer {
    private JSONObject digitalHomepageImpression;
    private JSONObject addToCartDigital;
}
