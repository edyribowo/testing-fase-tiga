package com.edyribowo.api.response.getprovider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    String name;
    String sku;
    String networkOperator;
    double nominal;
    double price;
    double offerPrice;
    double discountPercentage;
}
