package com.edyribowo.api.response.getprovider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProviderResponseData {
    private String provider;
    private List<Product> products;
}
