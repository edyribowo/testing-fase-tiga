package com.edyribowo.api.response.getcontent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.api.response.getcontent.GetContentDigitalProductResponse")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetContentDigitalProductResponse {
    private GetContentDigitalProductResponseData data;
}
