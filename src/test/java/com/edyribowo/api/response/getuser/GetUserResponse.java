package com.edyribowo.api.response.getuser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUserResponse {
    private GetUserResponseData data;
}
