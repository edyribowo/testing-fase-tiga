package com.edyribowo.api.response.getuser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUserResponseData {
    private String username;
    private String email;
    private String phoneNumber;
}
