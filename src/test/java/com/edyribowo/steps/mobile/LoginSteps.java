package com.edyribowo.steps.mobile;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.mobile.HomePage;
import static org.hamcrest.MatcherAssert.assertThat;

import com.edyribowo.module.ui.mobile.LoginPage;
import com.edyribowo.module.ui.mobile.ProfilePage;
import com.edyribowo.module.ui.mobile.VerificationPage;
import com.edyribowo.properties.UserDataProperties;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberDefinitionSteps
public class LoginSteps extends ScenarioSteps {
    @Autowired
    HomePage homePage;

    @Autowired
    ProfilePage profilePage;

    @Autowired
    UserDataProperties userDataProperties;

    @Autowired
    LoginPage loginPage;

    @Autowired
    VerificationPage verificationPage;

    @Given("^\\[mobile-apps\\] user Open \"([^\"]*)\" Application$")
    public void mobileAppsUserOpenApplication(String arg0) {
        homePage.waitUntilElementAppear();
    }

    @Then("^\\[mobile-apps\\] user on Profile page$")
    public void mobileAppsUserOnProfilePage() {
        assertThat("Wrong Page!", profilePage.isUserOnProfilPage());
    }

    @When("^\\[mobile-apps\\] user click Akun button$")
    public void mobileAppsUserClickAkunButton() {
        homePage.userClickAkunButton();
    }

    @When("^\\[mobile-apps\\] user click Masuk/Daftar$")
    public void mobileAppsUserClickMasukDaftar() {
        profilePage.clickMasukDaftar();
    }

    @And("^\\[mobile-apps\\] user type email$")
    public void mobileAppsUserTypeEmail() {
        loginPage.userTypeEmail(userDataProperties.getData().get("emailnonverified"));
    }

    @And("^\\[mobile-apps\\] user type password$")
    public void mobileAppsUserTypePassword() {
        loginPage.userTypePassword(userDataProperties.getData().get("passwordnonverified"));
    }

    @And("^\\[mobile-apps\\] user click Masuk button$")
    public void mobileAppsUserClickMasukButton() {
        loginPage.userClickMasukButton();
    }

    @And("^\\[mobile-apps\\] user click None Of The Above Google Smart Lock$")
    public void mobileAppsUserClickNoneOfTheAboveGoogleSmartLock() {
        loginPage.userClickNoneOfTheAboveGoogleSmartLock();
    }

    @Then("^\\[mobile-apps\\] user on verification page$")
    public void mobileAppsUserOnVerificationPage() {
        assertThat("Wrong page!", verificationPage.isUserOnVerificationPage());
    }

    @When("^\\[mobile-apps\\] user click Verification Later$")
    public void mobileAppsUserClickVerificationLater() {
        waitABit(3000);
        verificationPage.clickVerificationLater();
        verificationPage.clickVerificationLater();
    }

    @And("^\\[mobile-apps\\] user click Lanjutkan$")
    public void mobileAppsUserClickLanjutkan() {
        verificationPage.clickLanjutkanBtn();
    }

    @And("^\\[mobile-apps\\] username is correct$")
    public void mobileAppsUsernameIsCorrect() {
        System.out.println(userDataProperties.getData().get("emailnonverified")+" "+profilePage.getUsernameUser());
        assertThat("Username is incorrect!", profilePage.getUsernameUser().contains(userDataProperties.getData().get("emailnonverified")));
    }

    @When("^\\[mobile-apps\\] user click toogle in Profil$")
    public void mobileAppsUserClickToogleInProfil() {
        profilePage.clickToggle();
    }

    @And("^\\[mobile-apps\\] user click Keluar in Profil$")
    public void mobileAppsUserClickKeluarInProfil() {
        profilePage.clickLogOutButton();
    }

    @Then("^\\[mobile-apps\\] user click Keluar in PopUp$")
    public void mobileAppsUserClickKeluarInPopUp() {
        profilePage.clickLogOutButton();
    }

    @And("^\\[mobile-apps\\] user type (.*) and (.*)$")
    public void mobileAppsUserTypeEmailAndPassword(String email, String password) {
        loginPage.fillEmailAndPassword(email, password);
    }

    @Then("^\\[mobile-apps\\]  user able to see error message (.*)$")
    public void mobileAppsUserAbleToSeeErrorMessageErrorMessage(String error) {
        assertThat("Error message incorrect!", loginPage.cekErrorMessages(error));
    }

    @And("^\\[mobile-apps\\] user click back navigation$")
    public void mobileAppsUserClickBackNavigation() {
        loginPage.back();
    }
}
