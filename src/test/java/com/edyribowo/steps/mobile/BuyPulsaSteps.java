package com.edyribowo.steps.mobile;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.mobile.CategoryPage;
import com.edyribowo.module.ui.mobile.DigitalProductPage;
import com.edyribowo.module.ui.mobile.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;
import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class BuyPulsaSteps extends ScenarioSteps {
    @Autowired
    HomePage homePage;

    @Autowired
    CategoryPage categoryPage;

    @Autowired
    DigitalProductPage digitalProductPage;

    @And("^\\[mobile-apps\\] user click Semua on homepage$")
    public void mobileAppsUserClickSemuaOnHomepage() {
        homePage.clickSemuaCategory();
    }

    @Then("^\\[mobile-apps\\] user on Category page$")
    public void mobileAppsUserOnCategoryPage() {
        assertThat("Wrong page!", categoryPage.isUserOnCategoryPage());
    }

    @Then("^\\[mobile-apps\\] user on (.*) Page$")
    public void mobileAppsUserOnPage(String category) {
        assertThat("Wrong page!", digitalProductPage.isUserOnTheRightPage(category));
    }

    @When("^\\[mobile-apps\\] user input number with (.*)$")
    public void mobileAppsUserInputNumberWithPhoneNumber(String phoneNumber) {
        digitalProductPage.userTypePhoneNumber(phoneNumber);
    }

    @And("^\\[mobile-apps\\] users select nominal pulsa$")
    public void mobileAppsUserSelectNominalPulsaNavigation() {
        digitalProductPage.clickNominalProduct();
    }

    @And("^\\[mobile-apps\\] user select (.*)$")
    public void mobileAppsUserSelectNominal(String nominal) {
        digitalProductPage.selectPulsaWithNominal(nominal);
    }

    @And("^\\[mobile-apps\\] user click button \"([^\"]*)\"$")
    public void mobileAppsUserClickButton(String button) {
        digitalProductPage.userClickButton(button);
    }

    @Then("^\\[mobile-apps\\] user on Payment Pages$")
    public void mobileAppsUserOnPaymentPage() {
        assertThat("Wrong page!", digitalProductPage.isUserOnPaymentPage());
    }

    @And("^\\[mobile-apps\\] make sure the (.*) is correct$")
    public void mobileAppsMakeSureThePhoneNumberIsCorrect(String phoneNumber) {
        assertThat("Number incorrect", digitalProductPage.getPhoneNumber().contains(phoneNumber));
    }

    @When("^\\[mobile-apps\\] user click the (.*) icon$")
    public void mobileAppsUserClickTheIcon(String keyword) {
        categoryPage.clickicon(keyword);
    }

    @Then("^\\[mobile-apps\\] user see warning (.*)$")
    public void mobileAppsUserSeeWarningErrorMessage(String keyword) {
        assertThat("Wrong error messages!", digitalProductPage.getWarningShown(keyword).contains(keyword));
    }

    @When("^\\[mobile-apps\\] user click banner promo$")
    public void mobileAppsUserClickBannerPromo() {
        digitalProductPage.clickBannerPromo();
    }

    @Then("^\\[mobile-apps\\] user on promo page$")
    public void mobileAppsUserOnPromoPage() {
        digitalProductPage.isUserOnPromoPage();
    }

    @Then("^\\[mobile-apps\\] the (.*) should be appear in digital product page$")
    public void mobileAppsThePhoneNumberShouldBeAppearInDigitalProductPage(String phoneNumber) {
        assertThat("No Element!", digitalProductPage.isFormConsistOfPhoneNumber().equals(phoneNumber));
    }
}
