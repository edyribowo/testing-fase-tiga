package com.edyribowo.steps.mobile;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.mobile.HomePage;
import cucumber.api.java.en.Given;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberDefinitionSteps
public class TheAppSteps extends ScenarioSteps {

    @Autowired
    HomePage homePage;

    @Given("^user Open \"([^\"]*)\" Application$")
    public void userOpenApplication(String arg0) {
       homePage.waitUntilElementAppear();
    }
}
