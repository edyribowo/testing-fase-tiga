package com.edyribowo.steps.mobile;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.mobile.DigitalProductPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;
import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class VerifyComponentSteps extends ScenarioSteps {
    @Autowired
    DigitalProductPage digitalProductPage;

    @When("^\\[mobile-apps\\] user click phonebook icon on search bar$")
    public void mobileAppsUserClickPhonebookIconOnSearchBar() {
        digitalProductPage.clickPhoneBookIconOnSearchBar();
    }

    @Then("^\\[mobile-apps\\] the contact on a phone is appeared$")
    public void mobileAppsTheContactOnAPhoneIsAppeared() {
        assertThat("Component not appeared!", digitalProductPage.isContactListAppeared());
    }

    @When("^\\[mobile-apps\\] user click select (.*)$")
    public void mobileAppsUserClickSelectContactName(String contactName) {
        digitalProductPage.clickContactName(contactName);
    }

    @When("^\\[mobile-apps\\] user click semua-info component$")
    public void mobileAppsUserClickSemuaInfoComponent() {
        digitalProductPage.clickSemuaInfoComponent();
    }

    @Then("^\\[mobile-apps\\] user is on Digital Product FAQ Page$")
    public void mobileAppsUserIsOnDigitalProductFAQPage() {
        assertThat("Wrong page!", digitalProductPage.isUserOnDigitalProductFAQPage());
    }

    @And("^\\[mobile-apps\\] user click allow permission to access contact$")
    public void mobileAppsUserClickAllowPermissionToAccessContact() {
        digitalProductPage.clickAllowContactPermission();

    }
}
