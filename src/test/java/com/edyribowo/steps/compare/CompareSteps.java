package com.edyribowo.steps.compare;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.module.ui.CommonPage;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import com.edyribowo.module.ui.mobile.DigitalProductPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class CompareSteps extends ScenarioSteps {
    @Autowired
    PulsaPage pulsaPage;

    @Autowired
    DigitalProductPage digitalProductPage;

    @Autowired
    ProviderData providerData;

    @Autowired
    CommonPage commonPage;

    HashMap<String, String> dataTemp = new HashMap<String, String>();

    @Then("^user get size of (.*) from desktop$")
    public void pulsaPageUserGetSizeOfProductAvailabel(String type) {
        int productAvailableSize = pulsaPage.getSizeOfProductAvailable();
        System.out.println("Size Available Desktop : "+productAvailableSize);
        dataTemp.put(type, String.valueOf(productAvailableSize));
    }

    @Given("^prepare data to get providers with customer number (.*)$")
    public void prepareDataToGetProviderWithCustomerNumberCustomerNumber(String custNumber) {
        providerData.setCustomerNumber(custNumber);
    }

    @And("^prepare data to get providers with product type (.*)$")
    public void prepareDataToGetProviderWithProductTypeProductType(String prodType) {
        providerData.setProductType(prodType);
    }

    @Given("^prepare to read the excel file$")
    public void prepareToReadTheExcelFile() throws IOException {
        commonPage.prepareToReadExcelFile();
    }

    @When("^prepare to add (.*) data comparation (.*) on (.*)$")
    public void prepareToAddAvailablePulsaProductOnData(String dataType, String value, String key) throws IOException {
        int rowIndex = 0, columnIndex=0;
        if(dataType.equals("AvailableProduct"))
            rowIndex = 1;

        switch (key) {
            case "Data":
                columnIndex = 0;
                dataTemp.put(key, value);
                break;
            case "Provider":
                columnIndex = 1;
                dataTemp.put(key, value);
                break;
            case "API":
                columnIndex = 2;
                break;
            case "Desktop":
                columnIndex = 3;
                break;
            case "Mobile":
                columnIndex = 4;
                break;
        }
        System.out.println(key+" : "+dataTemp.get(value));
        commonPage.updateAvailableProductCell(rowIndex, columnIndex, dataTemp.get(value));
    }

    @Then("^user get size of (.*) from API$")
    public void userGetSizeOfAvProdAPIFromAPI(String type) {
        System.out.println(type+" : "+providerData.getSizeProductAvailable());
        dataTemp.put(type,String.valueOf(providerData.getSizeProductAvailable()));
    }

    @Then("^The (.*) from the three platform should be same$")
    public void theDataFromTheThreePlatformShouldBeSame(String data) throws IOException {
        assertThat("Data not same!", commonPage.compareData(data));
    }

    @Then("^user get size of (.*) from mobile$")
    public void userGetSizeOfAvProdMobileFromMobile(String type) {
        int productAvailabelMobile = digitalProductPage.getProductAvailabel();
        System.out.println("Size Available Mobile : "+productAvailabelMobile);
        dataTemp.put(type, String.valueOf(productAvailabelMobile));
    }
}
