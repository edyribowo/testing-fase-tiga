package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.controller.RepurchaseOrderController;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetRepurchaseOrderSteps {
    @Autowired
    ProviderData providerData;

    @Autowired
    RepurchaseOrderController repurchaseOrderController;

    @Given("^prepare data to get repurchased order with product type (.*)$")
    public void prepareDataToGetRepurchasedOrderWithProductTypeProductType(String productType) {
        providerData.setProductType(productType);
    }


    @When("^send get repurchased order request$")
    public void sendGetRepurchasedOrderRequest() {
        providerData.setRepurchaseOrderResponse(repurchaseOrderController.getRepurchaseOrder());
    }


    @Then("^get repurchased order status code must be '(\\d+)'$")
    public void getRepurchasedOrderStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                providerData.getRepurchaseOrderResponse().statusCode() , equalTo(statusCode));
    }

    @And("^the repurchased '(.*)' should be (.*)$")
    public void theRepurchasedCustomerIdShouldBeCustomerId(String key, String value) {
        assertThat(CommonConstant.getWrongData(key, value),
                providerData.getRepurchaseOrderResponse().jsonPath().get("data[0].data."+key).toString()
                , equalTo(value));
    }
}
