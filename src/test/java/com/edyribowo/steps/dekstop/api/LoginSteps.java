package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.UserData;
import com.edyribowo.controller.LoginController;
import com.edyribowo.controller.UsersController;
import com.edyribowo.module.ui.dekstop.LoginPage;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import com.google.common.collect.Maps;

@CucumberDefinitionSteps
public class LoginSteps extends ScenarioSteps {
    @Autowired
    LoginController loginController;

    @Autowired
    UsersController usersController;

    @Autowired
    LoginPage loginPage;

    @Autowired
    UserData userData;

    String token;
    Map<String, Object> loginData = Maps.newHashMap();

    @Given("^user prepare data username with (.*)$")
    public void userPrepareDataUsernameWithEmail(String email) {
        userData.setEmail(email);
    }

    @And("^user prepare data password with (.*)$")
    public void userPrepareDataPasswordWithPassword(String password) {
        userData.setPassword(password);
    }

    @And("^user send request data login$")
    public void userSendRequestDataLogin() throws IOException {
        loginData.put("username", userData.getEmail());
        loginData.put("password", userData.getPassword());
        userData.setSendRequestDataLoginResponse(loginController.postLoginData(loginData));
        userData.setCookies(userData.getSendRequestDataLoginResponse().getCookies());
    }

    @Then("^login status should be '(\\d+)'$")
    public void loginStatusShouldBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                userData.getSendRequestDataLoginResponse().statusCode() , equalTo(statusCode));
    }

    @When("^user get token from response$")
    public void userGetTokenFromResponse() {
        token = userData.getSendRequestDataLoginResponse().jsonPath().get("data.challenge.token").toString();
        System.out.println("token : "+token);
    }

    @And("^user send request token to request-challenge$")
    public void userSendRequestTokenToRequestChallenge() throws IOException {
         userData.setSendRequestChallengeCodeResponse(loginController.postTokenRequestChallenge(loginData));
    }

    @And("^\\[email\\] set email with existing email (.*) and (.*)$")
    public void emailSetEmailWithExistingEmailFutureedyGmailComAndAsdfghjkl(String email, String password) {
        waitABit(5000);
        loginPage.checkOnEmail("pop.gmail.com", "pop3", email, password);
    }

    @And("^set verification code to Payload$")
    public void setVerificationCodeToPayload() {
        loginData.put("token", this.token);
    }

    @Then("^request challenge-code status code should be '(.*)'$")
    public void requestStatusCodeShouldBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                userData.getSendRequestChallengeCodeResponse().statusCode() , equalTo(statusCode));
    }

    @When("^user send request data login after verification$")
    public void userSendRequestDataLoginAfterVerification() throws IOException {
        userData.setSendRequestSecondDataLoginResponse(loginController.postKodeVerification(loginData));
    }

    @When("^user get kode verifikasi from email$")
    public void userGetKodeVerifikasiFromEmail() {
        loginData.put("kode", loginPage.getKodeVerifikasi());
    }

    @Then("^request send kode verif status status code should be '(\\d+)'$")
    public void requestSendKodeVerifStatusStatusCodeShouldBe(int statusCode) {
        userData.setCookiesAfterVerification(userData.getSendRequestSecondDataLoginResponse().getCookies());
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                userData.getSendRequestSecondDataLoginResponse().statusCode() , equalTo(statusCode));
    }

    @Then("^the '(.*)' should be (.*)$")
    public void theIdShouldBeId(String key, String value) {
        assertThat(CommonConstant.getWrongData(key, value),
                userData.getSendRequestSecondDataLoginResponse().jsonPath().get("data."+key).toString()
                , equalTo(value));
    }

    @And("^the login status should be '(.*)'$")
    public void theLoginStatusShouldBeBAD_REQUEST(String status) {
        assertThat(CommonConstant.getWrongData("Status", status),
                userData.getSendRequestDataLoginResponse().jsonPath().get("status").toString()
                , equalTo(status));
    }
}
