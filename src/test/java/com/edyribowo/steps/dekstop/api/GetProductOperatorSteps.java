package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.controller.ProductOperatorController;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetProductOperatorSteps extends ScenarioSteps {

    @Autowired
    ProviderData providerData;

    @Autowired
    ProductOperatorController productOperatorController;

    @Given("^send request data with (.*)$")
    public void sendRequestDataWithProduct(String product) {
        providerData.setProductOperatorResponse(productOperatorController.getProductOperator(product));
    }

    @Then("^get response product operator status code must be '(\\d+)'$")
    public void getResponseProductOperatorStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                providerData.getProductOperatorResponse().statusCode() , equalTo(statusCode));
    }

    @And("^the size of data should be (.*)$")
    public void theSizeOfDataShouldBeNumber(int number) {
        List<String> resIDs = providerData.getProductOperatorResponse().jsonPath().get("data");
        assertThat(CommonConstant.WRONG_SIZE, resIDs.size()
                , equalTo(number));
    }
}
