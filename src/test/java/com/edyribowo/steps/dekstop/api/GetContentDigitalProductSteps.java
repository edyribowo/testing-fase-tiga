package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.response.getcontent.GetContentDigitalProductResponse;
import com.edyribowo.controller.ContentController;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetContentDigitalProductSteps {
    @Autowired
    ContentController contentController;

    Response response;

    @When("^send get content digital product request$")
    public void sendGetContentDigitalProductRequest() {
        response = contentController.getContent();
    }

    @Then("^get content digital product status code must be '(\\d+)'$")
    public void getContentDigitalProductStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE, response.statusCode(), equalTo(statusCode));
    }

    @And("^get content response equals with request$")
    public void getSingleUserResponseEqualsWithRequest() {
        GetContentDigitalProductResponse responseClass = response.getBody().as(GetContentDigitalProductResponse.class);

        assertThat("There isnt PHONE_CREDIT in content", responseClass.getData().getContent().contains("PHONE_CREDIT"));
        assertThat("There isnt DATA_PACKAGE in content", responseClass.getData().getContent().contains("DATA_PACKAGE"));
        assertThat("There isnt PHONE_POSTPAID in content", responseClass.getData().getContent().contains("PHONE_POSTPAID"));
    }
}
