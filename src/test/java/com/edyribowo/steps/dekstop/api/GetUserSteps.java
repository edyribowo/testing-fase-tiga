package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.controller.UsersController;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetUserSteps {
    @Autowired
    UsersController usersController;

    Response response;

    @Given("^user already login in Blibli$")
    public void userAlreadyLoginInBlibli() {
    }

    @When("^send get single user request$")
    public void sendGetSingleUserRequest() {
        response = usersController.getUser();
    }

    @Then("^get single user status code must be '(\\d+)'$")
    public void getSingleUserStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE, response.statusCode() , equalTo(statusCode));
    }
}
