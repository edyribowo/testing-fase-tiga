package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.api.data.UserData;
import com.edyribowo.controller.HistoryNumberController;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetHistoryPhoneNumberSteps extends ScenarioSteps {
    @Autowired
    UserData userData;

    @Autowired
    ProviderData providerData;

    @Autowired
    HistoryNumberController historyNumberController;

    @When("^send get history number request$")
    public void sendGetHistoryNumberRequest() {
        userData.setPurchaseHistoryNumber(historyNumberController.getHistoryNumber(providerData.getProductType()));
    }

    @Then("^get history number status code must be '(\\d+)'$")
    public void getHistoryNumberStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,
                userData.getPurchaseHistoryNumber().statusCode() , equalTo(statusCode));
    }

    @And("^the (.*) have to on list in data response$")
    public void thePhoneNumberHaveToOnListInDataResponse(String phoneNumber) {
        boolean status=false;
        List<String> resIDs = userData.getPurchaseHistoryNumber().jsonPath().get("data");
        for(int i=0; i<resIDs.size(); i++) {
            if(userData.getPurchaseHistoryNumber().jsonPath().get("data["+i+"]").toString().equals(phoneNumber)) {
                status=true;
                break;
            }
        }

        assertThat(CommonConstant.getWrongData("phoneNumber", phoneNumber), status);
    }
}
