package com.edyribowo.steps.dekstop.api;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.api.response.getprovider.GetProviderResponse;
import com.edyribowo.controller.ProviderController;
import com.edyribowo.properties.ProviderDataProperties;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class GetProviderSteps {
    @Autowired
    ProviderDataProperties providerDataProperties;

    @Autowired
    ProviderController providerController;

    @Autowired
    ProviderData providerData;

    Response response;

    @And("^prepare data to get provider with product type '<(.*)>'$")
    public void prepareDataToGetProviderWithProductTypeProductTypePulsa(String productType) {
        providerData.setProductType(providerDataProperties.getData().get(productType));
    }

    @Given("^prepare data to get provider with customer number '<(.*)>'$")
    public void prepareDataToGetProviderWithCustomerNumberCustomerNumber(String customNumber) {
        providerData.setCustomerNumber(providerDataProperties.getData().get(customNumber));
    }

    @When("^send get provider request$")
    public void sendGetProviderRequest() {
        response = providerController.getProvider();
    }

    @Then("^get provider status code must be '(\\d+)'$")
    public void getProviderStatusCodeMustBe(int statusCode) {
        assertThat(CommonConstant.WRONG_STATUS_CODE,response.statusCode() , equalTo(statusCode));
    }

    @And("^get provider response equals with request$")
    public void getProviderResponseEqualsWithRequest() {
        GetProviderResponse responseClass = response.getBody().as(GetProviderResponse.class);
        assertThat(CommonConstant.WRONG_PROVIDER,
                responseClass.getData().getProvider().equals(providerDataProperties.getData().get("providerName")));
    }

    @And("^user get size available product from API$")
    public void userGetSizeAvailableProductFromAPI() {
        GetProviderResponse responseClass = response.getBody().as(GetProviderResponse.class);
        System.out.println("Size : "+responseClass.getData().getProducts().size());
        providerData.setSizeProductAvailable(responseClass.getData().getProducts().size());
    }
}
