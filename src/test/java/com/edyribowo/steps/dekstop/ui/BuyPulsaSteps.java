package com.edyribowo.steps.dekstop.ui;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.dekstop.HomePage;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class BuyPulsaSteps extends ScenarioSteps
{
    @Autowired
    HomePage homePage;

    @Autowired
    PulsaPage pulsaPage;

    @Given("^\\[home-page\\] user open the blibli homepage$")
    public void homePageUserOpenTheBlibliHomepage() {
        homePage.open();
        System.out.println("open");
    }

    @When("^\\[home-page\\] user click the (.*) icon$")
    public void homePageUserClickThePulsaIcon(String iconSelected) {
        homePage.clickIconPulsa(iconSelected);
    }


    @Then("^\\[pulsa-page\\] user able to see url contains \"([^\"]*)\" in pulsa page$")
    public void pulsaPageUserAbleToSeeUrlContainsInPulsaPage(String pulsa)  {
        assertThat(CommonConstant.THE_URL_NOT_CONTAINS_PULSA,pulsaPage.isURLContainsPulsaString(pulsa));
    }

    @And("^\\[pulsa-page\\] user able to see form phone number in pulsa page$")
    public void pulsaPageUserAbleToSeeFormPhoneNumberInPulsaPage() {
        pulsaPage.isPulsaPageContainsPhoneNumberForm();
    }

    @When("^\\[pulsa-page\\] user fill the form (.*)$")
    public void pulsaPageUserFillTheFormPhoneNumber(String phoneNumber) {
        pulsaPage.inputPhoneNumber(phoneNumber);
    }


    @Then("^\\[pulsa-page\\] user able to see option nominal pulsa$")
    public void pulsaPageUserAbleToSeeOptionNominalPulsa() {
        assertThat(CommonConstant.ELEMENT_NOT_DISPLAYED,  pulsaPage.isPilihanPulsaDisplayed());
    }

    @When("^\\[pulsa-page\\] user choose nominal pulsa (.*)$")
    public void pulsaPageUserChooseNominalPulsa(String nominalPulsa)  {
        pulsaPage.userChooseNominalPulsa(nominalPulsa);
    }

    @And("^\\[pulsa-page\\] user click Beli Pulsa$")
    public void pulsaPageUserClickBeliPulsa() {
        pulsaPage.clickBtnBeliPulsa();
    }

    @Then("^\\[pulsa-page\\] user able to see error message (.*)$")
    public void pulsaPageUserAbleToSeeErrorMessageErrorMessage(String errorMsg) {
        assertThat(pulsaPage.getErrorMsg(), equalTo(errorMsg));
    }

    @And("^\\[pulsa-page\\] if any pop up, user close the pop up$")
    public void pulsaPageIfAnyPopUpUserCloseThePopUp() {
        pulsaPage.clickPopUpPromo();
    }

    @Then("^\\[pulsa-page\\] button Pay Now disabled$")
    public void pulsaPageButtonPayNowDisabled() {
        assertThat(CommonConstant.WRONG_ELEMENT_EXPECTED, pulsaPage.isButtonPayNowDisabled());
    }

    @And("^\\[pulsa-page\\] user click pulsa post-paid button$")
    public void pulsaPageUserClickPulsaPostPaidButton() {
        pulsaPage.clickPulsaPostpaidButton();
    }

    @When("^\\[home-page\\] user click Semua kategori button$")
    public void homePageUserClickSemuaKategoriButton() {
        homePage.clickSemuaKategori();
    }

    @When("^\\[pulsa-page\\] user click close verif number banner if any$")
    public void pulsaPageUserClickCloseVerifNumberBannerIfAny() {
        try {
            pulsaPage.clickCloseBannerVerificationNumber();
        }catch (Exception e) {
            System.out.println(e);
        }
    }
}
