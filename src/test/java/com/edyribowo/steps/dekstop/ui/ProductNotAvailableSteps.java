package com.edyribowo.steps.dekstop.ui;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import com.edyribowo.module.ui.mobile.DigitalProductPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.JavascriptExecutor;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class ProductNotAvailableSteps extends ScenarioSteps {
    @Autowired
    PulsaPage pulsaPage;

    @Then("^\\[pulsa-page\\] user able to see ProductNotAvailable Component$")
    public void homePageUserAbleToSeeProductNotAvailableComponent() {
        assertThat("Component is not available", pulsaPage.isProductNotAvailableComponentisAvailable());
    }

    @And("^\\[pulsa-page\\] user select (.*) on tab$")
    public void pulsaPageUserSelectProductOnTab(String product) {
        pulsaPage.selectProductOnTab(product);
    }
}
