package com.edyribowo.steps.dekstop.ui;
import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.UserData;
import com.edyribowo.module.ui.dekstop.HomePage;
import com.edyribowo.module.ui.dekstop.LoginPage;
import com.edyribowo.module.ui.dekstop.VerificationPage;
import com.edyribowo.properties.UserDataProperties;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class LoginSteps extends ScenarioSteps {

    @Autowired
    HomePage homePage;

    @Autowired
    VerificationPage verificationPage;

    @Autowired
    LoginPage loginPage;

    @Autowired
    UserDataProperties userDataProperties;

    @Autowired
    UserData userData;

    @Given("^\\[login-page\\] user open blibli login page$")
    public void homePageUserOpenBlibliLoginPage() {
        loginPage.open();
    }

    @When("^\\[login-page\\] user type (.*) and (.*)$")
    public void loginPageUserTypeUsernameAndPassword(String email, String password) {
        loginPage.fillPasswordAndEmail(email, password);
    }

    @And("^\\[login-page\\] user click login button$")
    public void loginPageUserClickLoginButton() {
        loginPage.clickBtnLogin();
    }

    @Then("^\\[verification-page\\] user able to see verif later link on verification page$")
    public void verificationPageUserAbleToSeeVerifLaterLinkOnVerificationPage() {
        assertThat(CommonConstant.THE_VERIF_LATER_LINK_NOT_SHOWN, verificationPage.isVerificationLaterLinkDisplayed());
    }

    @When("^\\[verification-page\\] user click verif later link$")
    public void verificationPageUserClickVerifLaterLink() {
        verificationPage.clickVerificationLaterLink();
    }

    @Then("^\\[verification-page\\] user be able to see popup verification$")
    public void verificationPageUserBeAbleToSeePopupVerification() {
        assertThat(CommonConstant.VERIF_POP_UP_NOT_SHOWN, verificationPage.isPopUpVerificationDisplayes());
    }

    @When("^\\[verification-page\\] user click lanjutkan$")
    public void verificationPageUserClickLanjutkan() {
        verificationPage.clickLanjutkanOnPopUpVerification();
    }

    @Then("^\\[homepage\\] user able to see the account name in homepage$")
    public void homepageUserAbleToSeeTheAccountNameInHomepage() {
        assertThat(CommonConstant.ACCOUNT_USER_IS_NOT_DISPLAYED,homePage.isAccountUserIsDisplayed());
    }

    @When("^\\[home-page\\] user click skip point$")
    public void homePageUserClickSkipPoint() {
        try {
            homePage.clickSkipPoint();
        } catch (Exception e) { }
    }

    @When("^\\[home-page\\] user click login button$")
    public void homePageUserClickLoginButton() {
        homePage.clickLoginBtn();
    }

    @Then("^\\[login-page\\] user able to see error message (.*)$")
    public void loginPageUserAbleToSeeErrorMessageErrorMessage(String errorMsg) {
        assertThat(CommonConstant.WRONG_ERROR_MESSAGES, loginPage.getErrorMsg().contains(errorMsg));
    }

    @And("^\\[login-page\\] user typing email and password$")
    public void loginPageUserTypingEmailAndPassword() {
        loginPage.fillPasswordAndEmail(userDataProperties.getData().get("emailnonverified"), userDataProperties.getData().get("passwordnonverified"));
    }

    @Given("^user already login in verification page$")
    public void userAlreadyLoginInVerificationPage() {
    }

    @And("^\\[home-page\\] user clear all cookie$")
    public void homePageUserClearAllCookie() {
        homePage.clearAllCookie();
    }

    @When("^\\[home-page\\] set cookie with gdn-value$")
    public void homePageSetCookieWithGdnValue() {
        homePage.setCookieWithGDNValue();
    }

    @And("^\\[home-page\\] user close the iframe if available$")
    public void homePageUserCloseTheIframeIfAvailable() {
        homePage.closeTheIframeIfAvailable();
    }

    @Then("^\\[login-page\\] button Pay Now disabled$")
    public void loginPageButtonPayNowDisabled() {
        assertThat("Login button is enabled!", loginPage.isLoginBtnDisabled());
    }

    @And("^\\[email\\] set email with email and password$")
    public void emailSetEmailWithEmailAndPassword() {
        waitABit(5000);
        loginPage.checkOnEmail("pop.gmail.com", "pop3", userDataProperties.getData().get("emailgmail"),
                userDataProperties.getData().get("passwordgmail"));
    }

    @And("^\\[login-page\\] user click send verification email button$")
    public void loginPageUserClickSendVerificationEmailButton() {
        loginPage.clickSendVerificationEmail();
    }

    @And("^\\[login-page\\] user fill verification code$")
    public void loginPageUserFillVerificationCode() {
        loginPage.userFillVerificationCode();
    }

    @And("^\\[login-page\\] user click verification$")
    public void loginPageUserClickVerification() {
        loginPage.userClickVerifNowBtn();
    }

    @And("^\\[home-page\\] user click Beri Izin Lokasi Button$")
    public void homePageUserClickBeriIzinLokasiButton() {
        homePage.clickBeriIjinLokasi();
    }

    @And("^\\[login-page\\] user storage the cookie$")
    public void loginPageUserStorageTheCookie() {
        loginPage.storageCookie(userDataProperties.getData().get("cookieName"));
    }

    @Given("^\\[home-page\\] use cookie from storage$")
    public void homePageUseCookieFromStorage() {
        loginPage.useCookie(userDataProperties.getData().get("cookieName"));
    }
}
