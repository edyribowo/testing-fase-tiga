package com.edyribowo.steps.dekstop.ui;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class HistoryOrderSteps extends ScenarioSteps {
    @Autowired
    PulsaPage pulsaPage;

    @And("^\\[pulsa-page\\] user can see the (.*) on history order$")
    public void pulsaPageUserCanSeeTheNumberOnHistoryOrder(String number) {
        assertThat("Element not found!", pulsaPage.isHistoryOrderVisible());
        assertThat("Number not correct!", pulsaPage.getTheNumberOrderHistory().contains(number));
    }

    @When("^\\[pulsa-page\\] click history order$")
    public void pulsaPageClickHistoryOrder() {
        pulsaPage.clickHistoryOrder();
    }

    @Then("^\\[pulsa-page\\] verify the number is (.*) on input form$")
    public void pulsaPageVerifyTheNumberIsNumberOnInputForm(String number) {
        assertThat("Number on input number is wrong!", pulsaPage.getValueOnInputForm().equals(number));
    }
}
