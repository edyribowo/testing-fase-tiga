package com.edyribowo.steps.dekstop.ui;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import cucumber.api.CucumberOptions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class PromoSteps extends ScenarioSteps {
    @Autowired
    PulsaPage pulsaPage;

    @And("^\\[pulsa-page\\] user click promo banner (.*)$")
    public void pulsaPageUserClickPromoBannerIndex(int index) throws InterruptedException {
        pulsaPage.clickBannerPromoWithIndex(index);
    }

    @Then("^\\[pulsa-page\\] verify the promo page url contains \"([^\"]*)\"$")
    public void pulsaPageVerifyThePromoPageUrlContains(String promo){
        pulsaPage.switchTabToNextTab(1);
        assertThat("Wrong url!", pulsaPage.getCurrentUrl().contains(promo));
    }
}
