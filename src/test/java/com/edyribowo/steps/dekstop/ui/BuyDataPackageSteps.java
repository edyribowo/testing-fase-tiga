package com.edyribowo.steps.dekstop.ui;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.module.ui.dekstop.HomePage;
import com.edyribowo.module.ui.dekstop.PaymentPage;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

@CucumberDefinitionSteps
public class BuyDataPackageSteps {
    @Autowired
    HomePage homePage;

    @Autowired
    PulsaPage pulsaPage;

    @Autowired
    PaymentPage paymentPage;

    @And("^\\[pulsa-page\\] user select sub menu paket data$")
    public void pulsaPageUserSelectSubMenuPaketData() {
        pulsaPage.clickSubMenuPaketData();
    }

    @Then("^\\[pulsa-page\\] user able to see option paket data$")
    public void pulsaPageUserAbleToSeeOptionPaketData() {
        assertThat(CommonConstant.OPTION_PAKET_DATA_IS_NOT_VISIBLE,  pulsaPage.isOptionPaketDataDisplayed());
    }

    @When("^\\[pulsa-page\\] user select (.*) in option$")
    public void pulsaPageUserSelectPaketDataInOption(String paketData) {
        pulsaPage.selectDataPackage(paketData);
    }

    @Then("^\\[payment-page\\] verify user able to see payment page$")
    public void paymentPageVerifyUserAbleToSeePaymentPage() {
        assertThat(CommonConstant.WRONG_PAGE, paymentPage.isUserOnPaymentPage());
    }

    @And("^\\[pulsa-page\\] user click phone credit tab$")
    public void pulsaPageUserClickPhoneCreditTab() {
        pulsaPage.clickPhoneCreditTab();
    }

    @When("^\\[home-page\\] user click (.*) kategori$")
    public void homePageUserClickSemuaKategori(String buttonType) {
        homePage.clickIconPulsa(buttonType);
    }
}
