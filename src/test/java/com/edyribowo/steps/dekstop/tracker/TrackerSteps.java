package com.edyribowo.steps.dekstop.tracker;

import com.edyribowo.annotation.CucumberDefinitionSteps;
import com.edyribowo.api.data.DigitalProductDataLayer;
import com.edyribowo.module.ui.dekstop.PulsaPage;
import com.edyribowo.utils.CommonConstant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@CucumberDefinitionSteps
public class TrackerSteps extends ScenarioSteps {
    @Autowired
    PulsaPage pulsaPage;

    @Autowired
    DigitalProductDataLayer digitalProductDataLayer;


    @When("^\\[tracker\\] user extract dataLayer (.*)$")
    public void trackerUserExtractDataLayerDigitalHomepageImpression(String event) throws JSONException {
        digitalProductDataLayer.setDigitalHomepageImpression(pulsaPage.extractDataLayer(event));
    }

    @Then("^\\[tracker\\] the '(.*)' should be (.*)$")
    public void trackerTheEventShouldBeDigitalHomepageImpression(String key, String value) throws JSONException {
        if(!"event".equals(key) && !"pageType".equals(key)) {
            assertThat(CommonConstant.getWrongData(key, value),
                    digitalProductDataLayer.getDigitalHomepageImpression().getJSONObject("eventDetails").get(key).toString()
                    , equalTo(value));
        } else {
            assertThat(CommonConstant.getWrongData(key, value),
                    digitalProductDataLayer.getDigitalHomepageImpression().get(key).toString()
                    , equalTo(value));
        }
    }

    @And("^\\[tracker\\] the event '(.*)' appears only (.*)$")
    public void trackerTheEventDigitalHomepageImpressionAppearsOnly(String event, int size) {
        waitABit(3000);
        assertThat(CommonConstant.WRONG_SIZE, pulsaPage.getSizeDataLayerOf(event),
                equalTo(size));
    }

    @And("^\\[home-page\\] user open digitalproduct page")
    public void homePageUserOpenDigitalproductUrlPage() {
        waitABit(2000);
        pulsaPage.open();
    }
}
