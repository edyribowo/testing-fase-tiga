package com.edyribowo.xpath.mobile;

public class MobileProfilePagePath {
    public static final String AKUN_TEXT_PATH = "//android.widget.TextView[@text='Akun']";
    public static final String MASUK_DAFTAR_LINK_PATH = "//android.widget.TextView[@text='Masuk/Daftar']";

    public static final String USERNAME_PATH = "(//android.widget.TextView)[1]";
    public static final String ICON_TOGGLE_PATH = "//android.widget.ImageView[@resource-id='blibli.mobile.commerce:id/iv_right_arrow']";
    public static final String LOGOUT_BUTTON_PATH = "//android.widget.Button[@text='Keluar']";

    public static String getErrorPath(String error) {
        return "//android.widget.TextView[@text='"+error+"']";
    }
}
