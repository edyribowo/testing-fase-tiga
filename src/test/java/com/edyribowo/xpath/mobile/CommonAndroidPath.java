package com.edyribowo.xpath.mobile;

public class CommonAndroidPath {
    public static final String CONTACT_PATH = "//android.widget.FrameLayout[@package='com.android.contacts']";

    public static String getContactNamePath(String contactName) {
        return "//android.widget.TextView[@text='"+contactName+"']";
    }
}
