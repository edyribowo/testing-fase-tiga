package com.edyribowo.xpath.mobile;

public class MobileVerificationPagePath {
    public static final String HEADER_VERIFICATION_PAGE_PATH = "//android.widget.TextView[@text='Verifikasi Nomor Handphone']";
    public static final String VERIF_LATER_LINK_PATH = "//android.widget.Button[@text='Nanti saja']";
    public static final String LANJUTAN_BUTTON_PATH = "//android.widget.Button[@text='LANJUTKAN']";
}
