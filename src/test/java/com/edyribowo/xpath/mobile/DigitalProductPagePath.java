package com.edyribowo.xpath.mobile;

public class DigitalProductPagePath {
    public static final String INPUT_NUMBER_PATH = "//android.widget.EditText";
    public static final String NOMINAL_PRODUCT_PATH = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/sp_recharge_list']";
    public static final String HEADER_PAYMENT_PAGE_PATH = "//android.widget.TextView[@text='Total pembayaran']";
    public static final String PHONE_NUMBER_PATH = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_numberEntered']";
    public static final String ERR0R_WRONG_PHONE_NUMBER = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_error_message']";
    public static final String BANNER_CAROUSEL_PROMO = "//android.widget.ImageView[@resource-id='blibli.mobile.commerce:id/iv_carousel_image']";
    public static final String PROMO_PAGE_HEADER = "//android.webkit.WebView[@resource-id='blibli.mobile.commerce:id/wv_anchor_store']";
    public static final String PHONE_BOOK_ICON = "//android.widget.ImageView[@resource-id='blibli.mobile.commerce:id/iv_phone_number']";
    public static final String SEMUA_INFO_PATH = "//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_faq_pulsa']";
    public static final String FAQ_PULSA_PATH = "//android.widget.TextView[@text='Pulsa – Pusat Bantuan']";
    public static final String CONTACT_PERMISSION_ALLOW_PATH = "//*[@class='android.widget.Button'][2]";
    public static final String PRODUCT_AVAILABLE_PATH = "//android.widget.TextView[@text='Harga']";

    public static String getHeaderPagePath(String category) {
        return "//android.widget.TextView[@text='"+category+"']";
    }

    public static String getSelectedNominal(String nominal) {
        return "//android.widget.TextView[@text='"+nominal+"']";
    }

    public static String getButtonPath(String button) {
        return "//android.widget.Button[@text='"+button+"']";
    }
}
