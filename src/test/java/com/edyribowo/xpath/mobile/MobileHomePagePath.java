package com.edyribowo.xpath.mobile;

import net.serenitybdd.core.pages.PageObject;

public class MobileHomePagePath {
    public static final String MOBILE_AKUN_BUTTON_PATH = "//android.widget.FrameLayout[@content-desc='Akun']";
    public static final String MOBILE_CATEGORY_BUTTON_PATH = "//android.widget.TextView[@text='Semua']";

}
