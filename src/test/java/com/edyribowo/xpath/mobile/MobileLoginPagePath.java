package com.edyribowo.xpath.mobile;

public class MobileLoginPagePath {
    public static final String INPUT_EMAIL_PATH = "//android.widget.EditText[@text='Email/nomor HP']";
    public static final String INPUT_PASSWORD_PATH = "//android.widget.EditText[@text='Kata sandi']";
    public static final String MASUK_BUTTON_PATH = "//android.widget.Button[@text='Masuk']";
    public static final String CLOSE_GOOGLE_SMART_LOCK_PATH = "//android.widget.Button[@text='NONE OF THE ABOVE']";
}
