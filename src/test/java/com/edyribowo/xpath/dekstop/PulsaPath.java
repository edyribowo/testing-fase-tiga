package com.edyribowo.xpath.dekstop;

public class PulsaPath {
    public static final String PHONE_NUMBER_FIELD = "//input[@type='tel']";
    public static final String TAB_ITEM_PHONE_CREDIT = "//div[@class='tabs__item b-active']";
    public static final String PAY_NOW_BUTTON = "//button[@id=\"btn-paynow\"]";
    public static final String ERROR_MESSAGES = "//span[@class=\"blu-field__msg\"]";
    public static final String ON_BOARDING_ELEMENT = "//span[@class=\"blu-field__msg\"]";
    public static final String POST_PAID_PHONE_CREDIT_BUTTON = "//div[@id='tab-PHONE_POSTPAID']";
    public static final String TAB_DATA_PACKAGE = "//a[contains(text(),'Paket Data')]";
    public static final String TAB_ITEM_DATA_PACKAGE = "//div[@id='DATA_PACKAGE']";
    public static final String PRODUCT_CARD_ITEM = "//div[@class='blu-card form__product']";
    public static final String AVAILABLE_PRODUCT_PATH = "//div[@class='tabs__item b-active']/div";
    public static final String PRE_PAID_PHONE_CREDIT_BUTTON = "(//div[@class='tabs-digital__header']//a)[2]";
    public static final String HISTORY_ORDER_PATH = "//div[@class='reorder-wrapper']";
    public static final String NUMBER_HISTORY_ORDER_PATH = "//div[@class='reorder-product_customerID']";
    public static final String NEXT_BANNER_BUTTON_PATH = "//span[@class='bli-down-arrow next-arrow']";
    public static final String PRODUCT_NOT_AVAILABLE_PATH = "//div[@class='product-not__available-img']";
    public static final String CLOSE_BANNER_VERIF_NUMBER = "//button[@class='blu-ticker__close b-order-right']";

    public static String getBannerPromoPath(int index) {
        return "(//div[@class='VueCarousel-slide']//img)["+index+"]";
    }

    public static String getProductTabPath(String product) {
        return "//div[contains(text(),'"+ product +"')]";
    }

    public static String getProductChosenPath(int idx) {
        return "(//div[@class='blu-card form__product'])["+idx+"]";
    }
}
