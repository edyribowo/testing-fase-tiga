package com.edyribowo.xpath.dekstop;

public class HomePagePath {
    public static final String ACCOUNT_NAME = "//div[@class='account__text']";
    public static final String ON_BOARDING_POINTS = "//button[@class='blu-btn b-ghost b-secondary b-small']";
    public static final String LOCATION_REQUEST_BUTTON = "//button[@class='blu-btn decline-btn b-outline b-secondary']";
    public static final String ALL_KATEGORI_BUTTON = "(//div[@class='favourite__item-label'])[1]";


    public static String getIconButtonPath(String iconSelected) {
        return "//div[contains(text(),'"+ iconSelected +"')]";
    }
}
