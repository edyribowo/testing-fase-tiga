package com.edyribowo.xpath.dekstop;

public class LoginPath {
    public static final String EMAIL_FIELD = "//input[@class='form__input login__username']";
    public static final String PASSWORD_FIELD = "//input[@class='form__input login__password']";
    public static final String LOGIN_PATH = "//div[@class='login__button']//button";
    public static final String ERROR_MESSAGES = "//div[@class='error']";
    public static final String BUTTON_VERIFICATION_PATH = "//button[@class='blu-btn otp-validation__button b-full-width b-secondary']";
    public static final String VERIF_INPUT_PATH = "//ul[@class='otp__codeField']/li/input";
    public static final String VERIF_NOW_BTN_PATH = "//div[@class='otp__confirm']//button";
}
