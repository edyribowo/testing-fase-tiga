package com.edyribowo.xpath.dekstop;

public class VerificationPath {
    public static final String VERIFICATION_LATER_LINK = "//button[@class='blu-btn footer__btn b-ghost b-secondary']";
    public static final String VERIFICATION_POP_UP = "//a[@class='modal-buttons__continue button modal-continue-button']";
}
