package com.edyribowo.module.ui.mobile;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

@Component("com.edyribowo.module.ui.mobile.TheAppPage")
public class TheAppPage extends PageObject {

    @FindBy(className = "android.widget.FrameLayout")
    private WebElementFacade txtChooseAnAwasomeView;

    @FindBy(xpath = "//android.widget.TextView")
    private List<WebElementFacade> menuList;

    public void waitUntilElementAppear(){
        txtChooseAnAwasomeView
                .withTimeoutOf(Duration.of(30,SECONDS))
                .waitUntilVisible();
    }

    public void clickOnMenu(String menuName){
        findAll(By.xpath("//android.widget.TextView")).stream().filter(webElementFacade -> webElementFacade.getText().toLowerCase().contains(menuName.toLowerCase())).findFirst().get().click();
    }
}
