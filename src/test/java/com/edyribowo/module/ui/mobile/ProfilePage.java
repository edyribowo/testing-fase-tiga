package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.mobile.Utility;
import com.edyribowo.xpath.mobile.MobileProfilePagePath;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.module.ui.mobile.ProfilePage")
public class ProfilePage extends Utility {
    public boolean isUserOnProfilPage() {
        scrollToElement(MobileProfilePagePath.AKUN_TEXT_PATH, 5);
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(MobileProfilePagePath.AKUN_TEXT_PATH));
        scrollDownTo();
        return isVisible(element);
    }

    public void clickMasukDaftar() {
        click(MobileProfilePagePath.MASUK_DAFTAR_LINK_PATH, 5);
    }

    public String getUsernameUser() {
        return getText(MobileProfilePagePath.USERNAME_PATH, 5);
    }

    public void clickToggle() {
        click(MobileProfilePagePath.ICON_TOGGLE_PATH, 5);
    }

    public void clickLogOutButton() {
        scrollToElement(MobileProfilePagePath.LOGOUT_BUTTON_PATH,20);
        click(MobileProfilePagePath.LOGOUT_BUTTON_PATH, 5);
    }
}
