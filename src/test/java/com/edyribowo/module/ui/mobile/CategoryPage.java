package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.mobile.Utility;
import com.edyribowo.xpath.mobile.MobileCategoryPagePath;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.module.ui.mobile.CategoryPage")
public class CategoryPage extends Utility {

    public boolean isUserOnCategoryPage() {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(MobileCategoryPagePath.HEADER_CATEGORY_PATH));
        return isVisible(element);
    }

    public void clickicon(String keyword) {
        click(MobileCategoryPagePath.getIconPath(keyword), 5);
    }
}
