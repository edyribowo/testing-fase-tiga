package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.mobile.MobileHomePagePath;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.module.ui.mobile.HomePage")
public class HomePage extends WebElementHelper {
    public void waitUntilElementAppear() {
        waitUntilVisible(MobileHomePagePath.MOBILE_AKUN_BUTTON_PATH);
    }

    public void userClickAkunButton() {
        click(MobileHomePagePath.MOBILE_AKUN_BUTTON_PATH);
    }

    public void clickSemuaCategory() {
        click(MobileHomePagePath.MOBILE_CATEGORY_BUTTON_PATH);
    }

}
