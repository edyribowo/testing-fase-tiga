package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.mobile.Utility;
import com.edyribowo.xpath.mobile.CommonAndroidPath;
import com.edyribowo.xpath.mobile.DigitalProductPagePath;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.edyribowo.module.ui.mobile.DigitalProductPage")
public class DigitalProductPage extends Utility {

    public boolean isUserOnTheRightPage(String category) {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(DigitalProductPagePath.getHeaderPagePath(category)));
        fluentWait(element, 10);
        return isVisible(element);
    }

    public void userTypePhoneNumber(String phoneNumber) {
        type(DigitalProductPagePath.INPUT_NUMBER_PATH, phoneNumber, 5);
    }

    public void clickNominalProduct() {
        click(DigitalProductPagePath.NOMINAL_PRODUCT_PATH, 5);
    }

    public void selectPulsaWithNominal(String nominal) {
        click(DigitalProductPagePath.getSelectedNominal(nominal),5);
    }

    public void userClickButton(String button) {
        click(DigitalProductPagePath.getButtonPath(button), 5);
    }

    public boolean isUserOnPaymentPage() {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(DigitalProductPagePath.HEADER_PAYMENT_PAGE_PATH));
        fluentWait(element, 5);
        return isVisible(element);
    }

    public String getPhoneNumber() {
        return getText(DigitalProductPagePath.PHONE_NUMBER_PATH, 5);
    }

    public String getWarningShown(String keyword) {
        return getText(DigitalProductPagePath.ERR0R_WRONG_PHONE_NUMBER, 5);
    }

    public void clickBannerPromo() {
        click(DigitalProductPagePath.BANNER_CAROUSEL_PROMO, 5);
    }

    public boolean isUserOnPromoPage() {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(DigitalProductPagePath.PROMO_PAGE_HEADER));
        fluentWait(element, 5);
        return isVisible(element);
    }

    public void clickPhoneBookIconOnSearchBar() {
        click(DigitalProductPagePath.PHONE_BOOK_ICON, 5);
    }

    public boolean isContactListAppeared() {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(CommonAndroidPath.CONTACT_PATH));
        fluentWait(element, 5);
        return isVisible(element);
    }

    public void clickContactName(String contactName) {
        click(CommonAndroidPath.getContactNamePath(contactName), 5);
    }

    public void clickSemuaInfoComponent() {
        click(DigitalProductPagePath.SEMUA_INFO_PATH, 5);
    }

    public boolean isUserOnDigitalProductFAQPage() {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(DigitalProductPagePath.FAQ_PULSA_PATH));
        fluentWait(element, 5);
        return isVisible(element);
    }

    public void clickAllowContactPermission() {
        click(DigitalProductPagePath.CONTACT_PERMISSION_ALLOW_PATH, 5);
    }

    public int getProductAvailabel() {
        String[] ListProduct;
        List<WebElementFacade> list = getListElement(DigitalProductPagePath.PRODUCT_AVAILABLE_PATH);
        System.out.println(list.toString());
        scrollUpTo();
        List<WebElementFacade> list1 = getListElement(DigitalProductPagePath.PRODUCT_AVAILABLE_PATH);
        System.out.println(list1.toString());
        return getListElement(DigitalProductPagePath.PRODUCT_AVAILABLE_PATH).size();
    }

    public String isFormConsistOfPhoneNumber() {
        return getText(DigitalProductPagePath.INPUT_NUMBER_PATH, 5);
    }
}
