package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.mobile.MobileVerificationPagePath;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("com.edyribowo.module.ui.mobile.VerificationPage")
public class VerificationPage extends WebElementHelper {

    public VerificationPage() throws IOException {
    }

    public boolean isUserOnVerificationPage() {
        return checkElementPresent(MobileVerificationPagePath.HEADER_VERIFICATION_PAGE_PATH);
    }

    public void clickVerificationLater() {
        click(MobileVerificationPagePath.VERIF_LATER_LINK_PATH);
    }

    public void clickLanjutkanBtn() {
        click(MobileVerificationPagePath.LANJUTAN_BUTTON_PATH);
    }
}
