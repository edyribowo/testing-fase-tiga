package com.edyribowo.module.ui.mobile;

import com.edyribowo.utils.mobile.Utility;
import com.edyribowo.xpath.mobile.MobileLoginPagePath;
import com.edyribowo.xpath.mobile.MobileProfilePagePath;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.module.ui.mobile.LoginPage")
public class LoginPage extends Utility {
    public void userTypeEmail(String emailnonverified) {
        type(MobileLoginPagePath.INPUT_EMAIL_PATH,emailnonverified, 5);
    }

    public void userTypePassword(String passwordnonverified) {
        type(MobileLoginPagePath.INPUT_PASSWORD_PATH, passwordnonverified, 5);
    }

    public void userClickMasukButton() {
        click(MobileLoginPagePath.MASUK_BUTTON_PATH, 5);
    }

    public void userClickNoneOfTheAboveGoogleSmartLock() {
        try {
            click(MobileLoginPagePath.CLOSE_GOOGLE_SMART_LOCK_PATH, 5);
        } catch (Exception e) {

        }
    }

    public void fillEmailAndPassword(String email, String password) {
        type(MobileLoginPagePath.INPUT_EMAIL_PATH,email, 5);
        type(MobileLoginPagePath.INPUT_PASSWORD_PATH, password, 5);
    }

    public boolean cekErrorMessages(String error) {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(MobileProfilePagePath.getErrorPath(error)));
        return isVisible(element);
    }
}
