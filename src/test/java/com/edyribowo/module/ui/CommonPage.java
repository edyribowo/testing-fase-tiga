package com.edyribowo.module.ui;

import com.edyribowo.utils.WebElementHelper;
import org.springframework.stereotype.Component;
import java.io.IOException;


@Component("com.edyribowo.module.ui.mobile.CommonPage")
public class CommonPage extends WebElementHelper {
    public void updateAvailableProductCell(int rowIndex, int columnIndex, String value) throws IOException {
        updateDataOnExcel(rowIndex,columnIndex,value);
    }
}
