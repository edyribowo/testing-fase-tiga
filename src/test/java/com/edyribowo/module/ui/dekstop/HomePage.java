package com.edyribowo.module.ui.dekstop;

import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.dekstop.HomePagePath;
import lombok.Data;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

@DefaultUrl("https://www.blibli.com/")
@Component
@Data
public class HomePage extends WebElementHelper {

    public boolean isAccountUserIsDisplayed() {
        return checkElementPresent(HomePagePath.ACCOUNT_NAME);
    }

    public void clickSkipPoint() {
        click(HomePagePath.ON_BOARDING_POINTS);
    }

    public void clickIconPulsa(String iconPulsa) {
        clickWithJs(HomePagePath.getIconButtonPath(iconPulsa));
    }

    public void clickLoginBtn() {
        clickWithJs(HomePagePath.getIconButtonPath("Masuk"));
    }

    public void closeTheIframeIfAvailable() {
        WebDriver driver = getDriver();
        try {
            driver.switchTo().frame(driver.findElement(By.xpath("/html[1]/body[1]/div[8]/iframe[1]")));
            driver.findElement(By.id("close-button-1454703945249")).click();
            driver.switchTo().defaultContent();
        } catch (Exception e) {

        }
    }

    public void clickBeriIjinLokasi() {
        try {
            click(HomePagePath.LOCATION_REQUEST_BUTTON);
        } catch (Exception e) {

        }
    }

    public void clickSemuaKategori() {
        clickWithJs(HomePagePath.ALL_KATEGORI_BUTTON);
    }
}
