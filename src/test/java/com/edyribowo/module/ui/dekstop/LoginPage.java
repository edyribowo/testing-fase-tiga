package com.edyribowo.module.ui.dekstop;
import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.dekstop.LoginPath;
import lombok.Data;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Cookie;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

@DefaultUrl("https://www.blibli.com/login?redirect=%2F")
@Data
@Component("com.edyribowo.module.ui.dekstop.LoginPage")
public class LoginPage extends WebElementHelper {
    String kodeVerification = "";

    public void fillPasswordAndEmail(String email, String password) {
        type(LoginPath.EMAIL_FIELD, email);
        type(LoginPath.PASSWORD_FIELD, password);
    }

    public void clickBtnLogin() {
        clickWithJs(LoginPath.LOGIN_PATH);
    }

    public String getErrorMsg() {
        return getText(LoginPath.ERROR_MESSAGES);
    }

    public void clickSendVerificationEmail() {
        clickWithJs(LoginPath.BUTTON_VERIFICATION_PATH);
    }

    public void checkOnEmail(String host, String mailStoreType, String emailgmail, String passwordgmail) {
        kodeVerification = checkEmail(host,mailStoreType,emailgmail,passwordgmail);
        System.out.println(kodeVerification);
    }

    public void userFillVerificationCode() {
        click(LoginPath.VERIF_INPUT_PATH);
        type(LoginPath.VERIF_INPUT_PATH, kodeVerification);
    }

    public void userClickVerifNowBtn() {
        click(LoginPath.VERIF_NOW_BTN_PATH);
    }

    public void storageCookie(String cookieName) {
        File file = new File(cookieName+".data");
        try
        {
            // Delete old file if exist
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter Bwrite = new BufferedWriter(fileWrite);
            // loop for getting the cookie information

            // loop for getting the cookie information
            for(Cookie ck : getDriver().manage().getCookies())
            {
                Bwrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                Bwrite.newLine();
            }
            Bwrite.close();
            fileWrite.close();

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void useCookie(String cookieName) {
        try{

            File file = new File(cookieName+".data");
            FileReader fileReader = new FileReader(file);
            BufferedReader Buffreader = new BufferedReader(fileReader);
            String strline;
            while((strline=Buffreader.readLine())!=null){
                StringTokenizer token = new StringTokenizer(strline,";");
                while(token.hasMoreTokens()){
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;

                    String val;
                    SimpleDateFormat sdf3 = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy",
                            Locale.ENGLISH);
                    if(!(val=token.nextToken()).equals("null"))
                    {
                        Date val1 = sdf3.parse(val);
                        expiry = val1;
                    }
                    Boolean isSecure = new Boolean(token.nextToken()).booleanValue();
                    Cookie ck = new Cookie(name,value,domain,path,expiry,isSecure);
                    System.out.println(ck);
                    getDriver().manage().addCookie(ck); // This will add the stored cookie to your current session
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public String getKodeVerifikasi() {
        return this.kodeVerification;
    }
}
