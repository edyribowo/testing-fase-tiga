package com.edyribowo.module.ui.dekstop;

import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.dekstop.VerificationPath;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class VerificationPage extends WebElementHelper {

    public void clickLanjutkanOnPopUpVerification() {

        click(VerificationPath.VERIFICATION_POP_UP);
    }

    public boolean isVerificationLaterLinkDisplayed() {
        return checkElementPresent(VerificationPath.VERIFICATION_LATER_LINK);
    }

    public void clickVerificationLaterLink() {
        clickWithJs(VerificationPath.VERIFICATION_LATER_LINK);
    }

    public boolean isPopUpVerificationDisplayes() {
        return checkElementPresent(VerificationPath.VERIFICATION_POP_UP);
    }


}
