package com.edyribowo.module.ui.dekstop;

import com.edyribowo.utils.WebElementHelper;
import lombok.Data;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

@DefaultUrl("https://www.blibli.com/login?redirect=%2F")
@Data
@Component("com.edyribowo.module.ui.dekstop.PaymentPage")
public class PaymentPage extends WebElementHelper {
    public boolean isUserOnPaymentPage() {
       waitABit(3000);
        return getCurrentUrl().contains("payment");
    }
}
