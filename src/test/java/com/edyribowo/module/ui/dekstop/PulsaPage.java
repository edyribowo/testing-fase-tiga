package com.edyribowo.module.ui.dekstop;

import com.edyribowo.utils.WebElementHelper;
import com.edyribowo.xpath.dekstop.PulsaPath;
import lombok.Data;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@DefaultUrl("https://www.blibli.com/digital/p")
@NamedUrls({
        @NamedUrl(name = "pulsa", url = "/pulsa"),
})
@Component
@Data
public class PulsaPage extends WebElementHelper {

    public boolean isURLContainsPulsaString(String pulsa) {
        return getCurrentUrl().contains(pulsa);
    }

    public boolean isPulsaPageContainsPhoneNumberForm() {
        return checkElementPresent(PulsaPath.PHONE_NUMBER_FIELD);
    }

    public void inputPhoneNumber(String phoneNumber) {
        type(PulsaPath.PHONE_NUMBER_FIELD, phoneNumber);
        clickTab(PulsaPath.PHONE_NUMBER_FIELD);
    }

    public boolean isPilihanPulsaDisplayed() {
        return checkElementPresent(PulsaPath.TAB_ITEM_PHONE_CREDIT);
    }

    public void userChooseNominalPulsa(String nominalPulsa) {
        List<WebElementFacade> elements = getListElement(PulsaPath.PRODUCT_CARD_ITEM);
        for (int i=0; i<elements.size();i++){
            if(elements.get(i).getText().contains(nominalPulsa)) {
                System.out.println(elements.get(i).getText());
                clickWithJs(PulsaPath.getProductChosenPath((i+1)));
                break;
            }
        }
    }

    public void clickBtnBeliPulsa() {
        clickWithJs(PulsaPath.PAY_NOW_BUTTON);
    }

    public String getErrorMsg() {
        return getText(PulsaPath.ERROR_MESSAGES);
    }

    public void clickPopUpPromo() {
        try {
            click(PulsaPath.ON_BOARDING_ELEMENT);
        } catch (Exception e){
            System.out.println(e.toString());
        }

    }

    public boolean isButtonPayNowDisabled() {
        return checkDisabledElement(PulsaPath.PAY_NOW_BUTTON);
    }

    public void clickPulsaPostpaidButton() {
        click(PulsaPath.POST_PAID_PHONE_CREDIT_BUTTON);
    }

    public void clickSubMenuPaketData() {
        clickWithJs(PulsaPath.TAB_DATA_PACKAGE);
    }

    public boolean isOptionPaketDataDisplayed() {
        return checkElementPresent(PulsaPath.TAB_ITEM_DATA_PACKAGE);
    }

    public void selectDataPackage(String paketData) {
        List<WebElementFacade> elements = getListElement(PulsaPath.PRODUCT_CARD_ITEM);
        for (WebElementFacade element : elements) {
            if (paketData.contains(element.getText())) {
                element.click();
                break;
            }
        }
    }

    public void clickPhoneCreditTab() {
        clickWithJs(PulsaPath.PRE_PAID_PHONE_CREDIT_BUTTON);
    }

    public boolean isHistoryOrderVisible() {
        return checkElementPresent(PulsaPath.HISTORY_ORDER_PATH);
    }

    public String getTheNumberOrderHistory() {
        return getText(PulsaPath.NUMBER_HISTORY_ORDER_PATH);
    }

    public void clickHistoryOrder() {
        click(PulsaPath.NUMBER_HISTORY_ORDER_PATH);
    }

    public String getValueOnInputForm() {
        return getValue(PulsaPath.PHONE_NUMBER_FIELD);
    }

    public void clickBannerPromoWithIndex(int index){
        clickWithJs(PulsaPath.getBannerPromoPath(index));
    }

    public void switchTabToNextTab(int index) {
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(index));
    }

    public boolean isProductNotAvailableComponentisAvailable() {
        return checkElementPresent(PulsaPath.PRODUCT_NOT_AVAILABLE_PATH);
    }

    public void selectProductOnTab(String product) {
        clickWithJs(PulsaPath.getProductTabPath(product));
    }

    public JSONObject extractDataLayer(String event) throws JSONException {
        System.out.println("Url : "+getCurrentUrl());
        int index=0;
        ArrayList<Object> dataLayer = (ArrayList<Object>) ((JavascriptExecutor) getDriver()).executeScript("return dataLayer;");
        for (int i=0; i<dataLayer.size(); i++) {
            Map<String, Object> map = (Map<String, Object>) dataLayer.get(i);
            if(map.containsValue(event)) {
                index=i;
                break;
            }
            index++;
        }
        Map<String, Object> result = (Map<String, Object>) ((JavascriptExecutor) getDriver()).executeScript("return dataLayer["+index+"];");

        JSONObject jsonObj = new JSONObject(result);
        return jsonObj;
    }

    public int getSizeDataLayerOf(String event) {
       int count=0;
        ArrayList<Object> dataLayer;

        try {
           dataLayer = (ArrayList<Object>) ((JavascriptExecutor) getDriver()).executeScript("return dataLayer;");
         } catch (Exception e) {
           getDriver().navigate().refresh();
           getDriver().switchTo().alert().accept();
           dataLayer = (ArrayList<Object>) ((JavascriptExecutor) getDriver()).executeScript("return dataLayer;");
        }

        System.out.println("Datalayer : "+dataLayer.toString());
       for (int i=0; i<dataLayer.size(); i++) {
           Map<String, Object> map = (Map<String, Object>) dataLayer.get(i);
           if(map.containsValue(event)) {
               count++;
               break;
           }
       }

       return count;
    }

    public void clickCloseBannerVerificationNumber() {
        click(PulsaPath.CLOSE_BANNER_VERIF_NUMBER);
    }

    public int getSizeOfProductAvailable() {
        return getListElement(PulsaPath.AVAILABLE_PRODUCT_PATH).size();
    }
}
