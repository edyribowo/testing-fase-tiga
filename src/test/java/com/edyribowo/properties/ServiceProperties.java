package com.edyribowo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@ConfigurationProperties(prefix = "service")
@Component("com.edyribowo.properties.ServiceProperties")
public class ServiceProperties {
    private HashMap<String, String> data;

}
