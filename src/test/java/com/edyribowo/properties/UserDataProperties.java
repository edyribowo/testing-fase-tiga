package com.edyribowo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@ConfigurationProperties(prefix = "user")
@Component("com.edyribowo.properties.UserDataProperties")
public class UserDataProperties {
    private HashMap<String, String> data;
}
