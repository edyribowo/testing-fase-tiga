package com.edyribowo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@ConfigurationProperties(prefix = "provider-data")
@Component("com.edyribowo.properties.ProviderDataProperties")
public class ProviderDataProperties {
    private HashMap<String, String> data;
}
