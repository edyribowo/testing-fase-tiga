package com.edyribowo.utils;

import com.edyribowo.xpath.dekstop.LoginPath;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import static java.time.temporal.ChronoUnit.SECONDS;

@Component("com.edyribowo.utils.WebElementHelper")
public class WebElementHelper extends PageObject {
    public void clickWithAction(String pathElement) {
        WebElementFacade element = find(By.xpath(pathElement));
        withAction().moveToElement(element).click().build().perform();
    }

    public void clickTab(String pathElement) {
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        element.sendKeys(Keys.TAB);
    }

    public boolean checkElementPresent(String pathElement) {
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        return element.isPresent();
    }

    public boolean checkDisabledElement(String pathElement) {
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        return element.isDisabled();
    }

    public void click(String pathElement) {
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        waitFor(ExpectedConditions.elementToBeClickable(By.xpath(pathElement)));
        element.click();
    }

    public void setCookieWithGDNValue() {
        Cookie ck = new Cookie("GDN-QA", "aef7531c0059d37bcf759e38070d68e44b0b5e2a");
        getDriver().manage().addCookie(ck);
    }

    public void clearAllCookie() {
        getDriver().manage().deleteAllCookies();
        waitABit(3000);
    }

    public void type(String pathElement, String value) {
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        element.type(value);
    }

    public String getText(String pathElement) {
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        return element.getText();
    }

    public String getCurrentUrl() {
        WebDriver webDriver = this.getDriver();
        waitABit(3000);
        return webDriver.getCurrentUrl();
    }

    public List<WebElementFacade> getListElement(String path) {
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));
        return withTimeoutOf(5, TimeUnit.SECONDS).findAll(By.xpath(path));
    }

    public void clickWithJs(String path) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0,1000)");
        WebElementFacade element = find(By.xpath(path));
        js.executeScript("arguments[0].click();", element);
    }

    public void scrollIntoView(String pathElement) throws InterruptedException {
        WebElementFacade elementFacade = find(By.xpath(pathElement));
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", elementFacade);
        Thread.sleep(500);
    }


    public void waitUntilVisible(String pathElement) {
        WebElementFacade element = find(By.xpath(pathElement));
        element.withTimeoutOf(Duration.of(120,SECONDS))
                .waitUntilVisible();
    }

    public String getValue(String pathElement) {
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(pathElement)));
        WebElementFacade element = find(By.xpath(pathElement));
        return element.getAttribute("value");
    }

    public boolean isLoginBtnDisabled() {
        return checkDisabledElement(LoginPath.LOGIN_PATH);
    }

    public String checkEmail(String host, String storeType, String username, String password) {
        String kode="";
        try {
            Properties properties = new Properties();
            properties.setProperty("mail.imap.ssl.enable", "true");

            Session emailSession = Session.getDefaultInstance(properties);
            Store store = emailSession.getStore("imap");
            store.connect(host, username, password);
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);
            Message[] messages = emailFolder.getMessages();

            Message message = messages[messages.length - 1];
            message.getSubject().contains("Verifikasi untuk masuk akun Blibli dari perangkat baru");

            kode = StringUtils.substringBetween(String.valueOf(message.getContent()), "<strong style=\"font-size:24px;\">", "</strong></td>");

            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kode;
    }

    public void prepareToReadExcelFile() throws IOException {
        FileInputStream file = new FileInputStream(new File("Data.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);

        try {
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                //For each row, iterate through each columns
                Iterator <Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue() + "\t\t");
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t\t");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "\t\t");
                            break;
                    }
                }
                System.out.println("");
            }
            file.close();
            FileOutputStream out =
                    new FileOutputStream(new File("Data.xls"));
            workbook.write(out);
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateDataOnExcel(int rowIndex, int columnIndex, String value) throws IOException {
        FileInputStream file = new FileInputStream(new File("Data.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Cell cell = null;

        try {
            //Update the value of cell
            cell = sheet.getRow(rowIndex).getCell(columnIndex);
            cell.setCellValue(value);
            file.close();

            FileOutputStream outFile = new FileOutputStream(new File("Data.xls"));
            workbook.write(outFile);
            outFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean compareData(String dataTemp) throws IOException {
        int idx = findIdxInExcel(dataTemp);
        System.out.println(dataTemp+" Index ke : "+idx);
        String dataDesktop = getValueFromExcel(idx, 3);
        String dataAPI = getValueFromExcel(idx, 2);
        String dataMobile = getValueFromExcel(idx, 4);

        if(dataDesktop.equals(dataAPI) && dataDesktop.equals(dataMobile)) {
            return true;
        } else {
            return false;
        }

    }

    private String getValueFromExcel(int idxRow, int idxColumn) throws IOException {
        FileInputStream file = new FileInputStream(new File("Data.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        String value = workbook.getSheetAt(0).getRow(idxRow).getCell(idxColumn).toString();
        return value;
    }

    private int findIdxInExcel(String dataTemp) throws IOException {
        FileInputStream file = new FileInputStream(new File("Data.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        int idx= 0;
        for (Row row : sheet) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());

                // get the text that appears in the cell by getting the cell value and applying any data formats (Date, 0.00, 1.23e9, $1.23, etc)
                String text = formatter.formatCellValue(cell);
                if (dataTemp.equals(text)) {
                    System.out.println("Text matched at " + cellRef.formatAsString());
                    break;
                }
            }
            idx++;
        }

        return (idx-1);
    }
}
