package com.edyribowo.utils;

import com.edyribowo.properties.ServiceProperties;
import com.hubspot.jinjava.Jinjava;
import io.restassured.http.ContentType;
import io.restassured.specification.FilterableRequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@Component("com.edyribowo.utils.ServisApi")
public class ServisApi {
    @Autowired
    ServiceProperties serviceProperties;

    public FilterableRequestSpecification service(String servisName) {
        FilterableRequestSpecification specification = (FilterableRequestSpecification) SerenityRest.given().
                header("User-Agent", "PostmanRuntime/7.24.0").
                header("Accept", "application/json").
                cookie("GDN-QA","0974f801eaf3e7e72510967ca801395e7d01cda0").
                contentType(ContentType.JSON).
                when();
        this.addDefaultBaseUrl(servisName, specification);;
        return specification;
    }

    private void addDefaultBaseUrl(String servisName, FilterableRequestSpecification specification) {
        String baseUrl = serviceProperties.getData().get(servisName);
        specification.baseUri(baseUrl);
    }

    public String replaceValueJson(String path, Map<String, Object> hashMap) {
        String renderedTemplate = "";
        try {
            Jinjava jinjava = new Jinjava();
            String template = generateStringFromResource(path);
            renderedTemplate = jinjava.render(template, hashMap);
        } catch (Exception e) {
            System.out.println(e);
        }
        return renderedTemplate;
    }

    public String generateStringFromResource(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }
}
