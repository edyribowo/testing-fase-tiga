package com.edyribowo.utils;

public class CommonConstant {

    // Error Message
    public static final String THE_VERIF_LATER_LINK_NOT_SHOWN = "The verification later link not shown.";
    public static final String VERIF_POP_UP_NOT_SHOWN = "The verification pop up not shown.";
    public static final String ACCOUNT_USER_IS_NOT_DISPLAYED = "The user account is not displayed";
    public static final String THE_URL_NOT_CONTAINS_PULSA = "The URL is not containing pulsa word";
    public static final String OPTION_PAKET_DATA_IS_NOT_VISIBLE = "Option paket data is not displayed";
    public static final String WRONG_PAGE = "User on wrong page!";
    public static final String WRONG_PROVIDER = "Wrong provider!";
    public static final String WRONG_STATUS_CODE = "Wrong status Code!";
    public static final String WRONG_ERROR_MESSAGES = "Wrong Error Messages!";
    public static final String ELEMENT_NOT_DISPLAYED = "Element not displayed";
    public static final String WRONG_ELEMENT_EXPECTED = "Element action wrong expected";
    public static final String WRONG_SIZE = "Wrong Size of Array!";

    public static String getWrongData(String key, String value) {
        return "The "+key+" should be : "+value;
    }
}
