package com.edyribowo.utils.mobile;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Utility extends PageObject {
    public int startx, starty, endx;

    public static AndroidDriver getCurrentDriver(){
        return (AndroidDriver) ((WebDriverFacade) ThucydidesWebDriverSupport.getDriver()).getProxiedDriver();
    }

    public void click(String pathElement, int timeOut) {
        scrollToElement(pathElement, 5);
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(pathElement));
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
        element.click();
    }

    public void clickWithIndex(List<WebElementFacade> elements, int index, int timeOut) {
        fluentWait(elements.get(index), timeOut);
        elements.get(index).click();
    }

    public List<WebElementFacade> getListElement(String path) {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(path));
        scrollToBottom();
        return withTimeoutOf(5, TimeUnit.SECONDS).findAll(net.serenitybdd.core.annotations.findby.By.xpath(path));
    }

    public void type(String pathElement, String text, int timeOut) {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(pathElement));
//        scrollToElement(pathElement, 5);
        fluentWait(element, timeOut);
        element.type(text);
    }

    public void fluentWait(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
    }

    public void clickByText(String text, int timeOut) {
        waitForElementToBeClickable(By.xpath("//*[@text='" + text + "']"), timeOut);
        getCurrentDriver().findElement(By.xpath("//*[@text='" + text + "']")).click();
    }

    public void waitForElementToBeClickable(By by, int timeOut) {
        WebDriverWait wait = new WebDriverWait(getCurrentDriver(), timeOut);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void back() {
        getCurrentDriver().navigate().back();
    }

    public String getText(String pathElement, int timeOut) {
        WebElementFacade element = find(net.serenitybdd.core.annotations.findby.By.xpath(pathElement));
        fluentWait(element, timeOut);
        return element.getText();
    }

    public String getTextWithIndex(List<WebElementFacade> elements, int index, int timeOut) {
        fluentWait(elements.get(index), timeOut);
        return elements.get(index).getText();
    }

    public void initWidthSize() {
        Dimension size = getCurrentDriver().manage().window().getSize();
        startx = (int) (size.width * 0.90);
        endx = (int) (size.width * 0.10);
        starty = size.height / 2;
    }

    public void swiperToLeft() {
        initWidthSize();
        swipe(startx, starty, endx, starty, 1000);
    }

    public static void scrollUpTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.5;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollStart, 0, scrollEnd, 2000);
    }

    public static void scrollDownTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.5;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollEnd, 0, scrollStart, 2000);
    }

    public void scrollDownToElement(String path, int count) {
        WebElementFacade elementIndicator = find(net.serenitybdd.core.annotations.findby.By.xpath(path));
        int i = 0;
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollDownTo();
            }
            i++;
        } while (i <= count);
    }

    public static void swipe(int startx, int starty, int endx, int endy, int duration) {
        System.out.println(
                "Swipe (" + startx + "," + starty + "," + endx + "," + endy + "," + duration + ")");
        new TouchAction((PerformsTouchActions) getCurrentDriver()).press(new PointOption().withCoordinates(startx, starty))
                .waitAction(new WaitOptions().withDuration(Duration.ofMillis(duration)))
                .moveTo(new PointOption().withCoordinates(endx, endy)).release().perform();
    }

    public void scrollToElement(String path, int count) {
        int i = 0;
        WebElementFacade elementIndicator = find(net.serenitybdd.core.annotations.findby.By.xpath(path));
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollUpTo();
            }
            i++;
        } while (i <= count);
    }

    public void waitForElementToBeClickable(WebElementFacade element) {
        WebDriverWait wait = new WebDriverWait(getCurrentDriver(), 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void implicitWait(int time) {
        getCurrentDriver().manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    public boolean isVisible(WebElementFacade element) {
        return element.isVisible();
    }

    public boolean isTextVisible(String text) {
        return getCurrentDriver().findElement(By.xpath("//*[@text='" + text + "']")).isDisplayed();

    }

    public void clearText(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
        element.clear();
    }

    public int getSize(List<WebElementFacade> elements) {
        return elements.size();
    }

    public static int randomNumberGenerator() {
        Random rand = new Random();
        int rand_int1 = rand.nextInt(10000000);
        return rand_int1;
    }

    public static void scrollToElementByText(String text, int count) {
        int i = 0;
        do {
            try {
                getCurrentDriver().findElement(By.xpath("//*[@text='" + text + "']")).isDisplayed();
                break;
            } catch (Exception e) {
                scrollUpTo();
            }
            i++;
        } while (i <= count);
    }

    public void clickByTextContains(String text, int timeOut) {
        waitForElementToBeClickable(By.xpath("(//android.widget.TextView[contains(@text," + text + ")])"), timeOut);

        getCurrentDriver().findElement(By.xpath("(//android.widget.TextView[contains(@text," + text + ")])")).click();
    }

    public String checkAttribute(WebElementFacade element, String attribute) {
        return element.getAttribute(attribute);
    }

    public void scrollToBottom() {
        int  x = getCurrentDriver().manage().window().getSize().width / 2;
        int start_y = (int) (getCurrentDriver().manage().window().getSize().height * 0.2);
        int end_y = (int) (getCurrentDriver().manage().window().getSize().height * 0.8);
        TouchAction dragNDrop = new TouchAction(getCurrentDriver())
                .press(PointOption.point(x,start_y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(x, end_y))
                .release();
        dragNDrop.perform();
    }
}
