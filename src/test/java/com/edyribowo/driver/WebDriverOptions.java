package com.edyribowo.driver;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverOptions {
    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

    public ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--window-size=1920,1080");
        //options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.merge(getDesiredCapabilities());

        //pass the security on headless
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36";
        options.addArguments(String.format("user-agent=%s", userAgent));
        return options;
    }

    public FirefoxOptions getFirefoxOptions() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        //firefoxOptions.addArguments("-headless");
        //firefoxOptions.setCapability("marionette", true);
        firefoxOptions.addArguments("--window-size").addArguments("1920,1080");
        firefoxOptions.merge(getDesiredCapabilities());
        return firefoxOptions;
    }

    public OperaOptions getOperaOptions() {
        OperaOptions operaOptions = new OperaOptions();
        operaOptions.addArguments("--window-size").addArguments("1920,1080");
        operaOptions.merge(getDesiredCapabilities());
        return operaOptions;
    }

    public DesiredCapabilities getDesiredCapabilities() {
        boolean video = false;
        if(variables.getProperty("enabledVideo").equals("true"))  {
            video = true;
        }

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(variables.getProperty("localBrowser"));
        capabilities.setVersion(variables.getProperty("browserVersion"));
        capabilities.setCapability("enableVnc", true);
        capabilities.setCapability("enableVideo", video);
        capabilities.setCapability("name", variables.getProperty("automationName"));

        return capabilities;
    }


}
