package com.edyribowo.driver;

import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class BaseDriver extends WebDriverOptions implements DriverSource {

    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    String browser = variables.getProperty("localBrowser");
    WebDriver webDriver;

    @SneakyThrows
    @Override
    public WebDriver newDriver() {
        if(variables.getProperty("isRunAndroid").equals("true")) {
            EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setCapability("platformName", variables.getProperty("platformName"));
            desiredCapabilities.setCapability("automationName", "UiAutomator2");
            desiredCapabilities.setCapability("udid", variables.getProperty("udid"));
            desiredCapabilities.setCapability("appPackage", "blibli.mobile.commerce");
            desiredCapabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");
            desiredCapabilities.setCapability("noReset", false);
            desiredCapabilities.setCapability("systemPort", variables.getProperty("systemPort"));

            URL url = null;
            try {
                url = new URL("http://127.0.0.1:"+variables.getProperty("port")+"/wd/hub");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            return new AndroidDriver<>(url, desiredCapabilities);
        }


        if(variables.getProperty("isRunWebApp").equals("true")) {
            if(variables.getProperty("isRemote").equals("false")) {
                if(browser.equals("chrome")) {
                    WebDriverManager.chromedriver().setup();
                    ChromeDriver chromeDriver = new ChromeDriver(getChromeOptions());
                    chromeDriver.manage().window().maximize();
                    System.out.println(chromeDriver);
                    return chromeDriver;
                } else if(browser.equals("firefox")) {
                    WebDriverManager.firefoxdriver().setup();
                    FirefoxDriver firefoxDriver= new FirefoxDriver(getFirefoxOptions());
                    firefoxDriver.manage().window().maximize();
                    return firefoxDriver;
                } else {
                    WebDriverManager.operadriver().setup();
                    OperaDriver operaDriver = new OperaDriver();
                    operaDriver.manage().window().maximize();
                    return operaDriver;
                }
            } else {
                if(browser.equals("chrome")) {
                    System.out.println("LOL : "+getChromeOptions());
                    webDriver = new RemoteWebDriver(
                            URI.create(variables.getProperty("selenoidHostUrl")).toURL(),
                            getChromeOptions()
                    );
                } else if (browser.equals("firefox")) {
                    webDriver = new RemoteWebDriver(
                            URI.create(variables.getProperty("selenoidHostUrl")).toURL(),
                            getFirefoxOptions()
                    );
                } else {
                    System.out.println(getOperaOptions());
                    webDriver = new RemoteWebDriver(
                            URI.create(variables.getProperty("selenoidHostUrl")).toURL(),
                            getOperaOptions()
                    );
                }

                return webDriver;
            }
        }

        return null;
    }


    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
