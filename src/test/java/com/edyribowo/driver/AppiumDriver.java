package com.edyribowo.driver;

import io.appium.java_client.android.AndroidDriver;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AppiumDriver implements DriverSource {

    @Override
    public WebDriver newDriver() {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", variables.getProperty("platformName"));
        desiredCapabilities.setCapability("automationName", "UiAutomator2");
        desiredCapabilities.setCapability("udid", variables.getProperty("udid"));
        desiredCapabilities.setCapability("appPackage", "blibli.mobile.commerce");
        desiredCapabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");
        desiredCapabilities.setCapability("noReset", false);
        desiredCapabilities.setCapability("systemPort", variables.getProperty("systemPort"));

        URL url = null;
        try {
            url = new URL("http://127.0.0.1:"+variables.getProperty("port")+"/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return new AndroidDriver<>(url, desiredCapabilities);
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}