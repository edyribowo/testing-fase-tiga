package com.edyribowo.controller;

import com.edyribowo.api.data.UserData;
import com.edyribowo.utils.ServisApi;
import org.openqa.selenium.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

@Component("com.edyribowo.controller.UsersController")
public class UsersController extends ServisApi {
    @Autowired
    UserData userData;

    public Response getUser() {
        Response response = service("blibli").
                cookies(userData.getCookiesAfterVerification()).
                get("/common/users");
        response.getBody().prettyPrint();
        return response;
    }

    public List<io.restassured.http.Cookie> getCookies() {
        Set<Cookie> seleniumCookies = getDriver().manage().getCookies();
        List<io.restassured.http.Cookie> restAssuredCookies = new ArrayList<>();
        for (org.openqa.selenium.Cookie cookie : seleniumCookies)
            restAssuredCookies.add(new io.restassured.http.Cookie.Builder(cookie.getName(), cookie.getValue()).build());
        return restAssuredCookies;
    }
}
