package com.edyribowo.controller;
import com.edyribowo.api.data.ProviderData;
import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.controller.ProviderController")
public class ProviderController extends ServisApi {
    @Autowired
    ProviderData providerData;

    public Response getProvider() {
        Response response = service("blibli").
                queryParam("productType",providerData.getProductType()).
                queryParam("customerNumber", providerData.getCustomerNumber()).
                get("/digital-product/products");
        response.getBody().prettyPrint();
        return response;
    }
}
