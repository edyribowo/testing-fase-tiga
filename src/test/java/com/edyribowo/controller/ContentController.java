package com.edyribowo.controller;

import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.controller.ContentController")
public class ContentController extends ServisApi {
    public Response getContent() {
        Response response = service("blibli").
                get("/common/content/digital-products");
        response.getBody().prettyPrint();
        return response;
    }

}
