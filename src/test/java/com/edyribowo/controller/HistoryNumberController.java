package com.edyribowo.controller;

import com.edyribowo.api.data.UserData;
import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.controller.HistoryNumberController")
public class HistoryNumberController extends ServisApi {
    @Autowired
    UserData userData;

    public Response getHistoryNumber(String productType) {
        Response response = service("blibli").
                cookies(userData.getCookiesAfterVerification()).
                get("/digital-product/members/number-history/"+productType);
        response.getBody().prettyPrint();
        return response;
    }
}
