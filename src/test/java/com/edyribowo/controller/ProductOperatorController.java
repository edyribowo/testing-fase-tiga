package com.edyribowo.controller;

import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.controller.ProductOperatorController")
public class ProductOperatorController extends ServisApi {
    public Response getProductOperator(String product) {
        Response response = service("blibli").
                get("/digital-product/operators/"+product);
        response.getBody().prettyPrint();
        return response;
    }
}
