package com.edyribowo.controller;

import com.edyribowo.api.data.ProviderData;
import com.edyribowo.api.data.UserData;
import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.edyribowo.controller.RepurchaseOrder")
public class RepurchaseOrderController extends ServisApi {
    @Autowired
    ProviderData providerData;

    @Autowired
    UserData userData;

    public Response getRepurchaseOrder() {
        Response response = service("blibli").
                cookies(userData.getCookiesAfterVerification()).log().all().
                queryParam("productType",providerData.getProductType()).
                get("/digital-product/orders/get-repurchase-orders");
        response.getBody().prettyPrint();
        return response;
    }
}
