package com.edyribowo.controller;

import com.edyribowo.api.data.UserData;
import com.edyribowo.utils.ServisApi;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component("com.edyribowo.controller.LoginController")
public class LoginController extends ServisApi {
    @Autowired
    UserData userData;

    public Response postLoginData(Map<String, Object> loginData) throws IOException {
        String pathJSON = "./JSONTemplates/_loginBeforeVerification.json";
        Response response = service("blibli").
                body(replaceValueJson(pathJSON, loginData)).log().all().
                post("/common/users/_login");
        response.getBody().prettyPrint();
        return response;
    }

    public Response postTokenRequestChallenge(Map<String, Object> loginData) throws IOException {
        String pathJSON = "./JSONTemplates/requestChallengeToken.json";
        Response response = service("blibli").
                body(replaceValueJson(pathJSON, loginData)).log().all().
                post("/common/users/_request-challenge-code");
        response.getBody().prettyPrint();
        return response;
    }

    public Response postKodeVerification(Map<String, Object> loginData) throws IOException {
        String pathJSON = "./JSONTemplates/_loginAfterVerification.json";
        Response response = service("blibli").
                cookies(userData.getCookies()).
                body(replaceValueJson(pathJSON, loginData)).log().all().
                post("/common/users/_login");
        response.getBody().prettyPrint();
        return response;
    }
}
